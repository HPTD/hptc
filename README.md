# HPTC

High-Precision Timing Clock generator board for timing characterization

![HPTC](altium_project/board_picture.png)

An overview of the board performance can be observed below

![HPTC](altium_project/hptc_151242626902_out0_lvpecl.png)

For more information, please check the hptc_characterization.pdf file

Contact us if you wish to order a sample:

[Order a new sample](mailto:dahernan@cern.ch,sophie.baron@cern.ch?subject=[HPTC%20Order])
