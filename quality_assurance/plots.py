#!/usr/bin/env python

####################################################
###              Python Packages                 ###
####################################################
import logging
import time
import datetime
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import sys
import math
import csv

# General phase-noise reading function
def read_phase_noise_fswp(filename, scale_carrier = 0, new_f_carrier = 40e6, fmin=0, fmax = 999e9):
    with open(filename + '.csv', 'r') as f:
        data = f.read()

    data = data.replace('\n','')
    # Look for carrier frequency
    f_carrier = float(((data.split('Signal Frequency')[1]).split(';'))[1])
    original_carrier = f_carrier
    # Look for 
    data = data.split('Values')
    data = data[1].split(';')

    freq = []
    pn   = []
    for i in range(2,len(data)-1):
        if(i%2 == 0) :
            freq.append(float(data[i]))
        else         :
            pn.append(float(data[i]))
            if(scale_carrier):
                pn[-1] = pn[-1] + 20*math.log10(new_f_carrier/f_carrier)

    if(scale_carrier):
        f_carrier = new_f_carrier
		
    # Integration
    int_jitter = 0
    for i in range(2,len(freq)):
        if(freq[i] > fmin and freq[i]<fmax):
            int_jitter = int_jitter + 0.5*((10**(pn[i]/10)) + (10**(pn[i-1]/10)))*(freq[i]-freq[i-1])
    int_jitter =  (1/(2*math.pi*f_carrier))*math.sqrt(2*int_jitter)
    int_bdw = freq[-1] - freq[0]

    return [freq, pn, int_jitter*1e12, int_bdw, original_carrier]

# General histogram reading function
def read_histogram(filename, zero_mean=0):
    with open(filename + '.txt', 'r') as f:
        data = f.read()

    data = data.split('\n')

    # Find start of phase noise values
    k=0
    while(data[k][0]=='#'):
        if (data[k].find('RMS (ps)') != -1):
            jitter = float(data[k].split(',')[1])
        if (data[k].find('XOrigin (ps)') != -1):
            xorigin = float(data[k].split(',')[1])
        if (data[k].find('Resolution (ps)') != -1):
            resolution = float(data[k].split(',')[1])	
        if (data[k].find('Mean (ps)') != -1):
            mean = float(data[k].split(',')[1])	
        k+=1

    # Take time and histogram values
    timex = []
    hist  = []
    if(zero_mean) : xorigin = xorigin - mean
    for i in range(k, len(data)-1):
        timex.append(xorigin + (i-k)*resolution)
        hist.append(float(data[i]))
    return [timex, hist, jitter, mean]


	
# General scope measurement reading function
def read_measurement_scope(filename, measurement_name='Edge-Edge(1-2)_AVG', until_point=1e9):
    with open(filename, 'r') as f:
        data = f.read()
    data   = data.split('\n')
    header = data[0].split(',')
    found_measurement=0
    for i in range(0,len(header)):    
        if measurement_name in header[i]:
            index_measurement = i
            found_measurement = 1
        elif('date' in header[i]):
            index_date = i
        elif('time' in header[i]):
            index_time = i

    if(not found_measurement) :
        print('--- Measurement not found')
        return [-1]

    measurement = []
    time_sample   = []
    for i in range(1,min(len(data)-1, until_point)):
        aux = data[i].split(',')
        measurement.append(float(aux[index_measurement]))

    return measurement

# General scope measurement reading function
def read_measurement_power(filename, measurement_name='Current(A)', until_point=1e9):
    with open(filename, 'r') as f:
        data = f.read()
    data   = data.split('\n')
    header = data[0].split(',')
    found_measurement=0
    for i in range(0,len(header)):    
        if measurement_name in header[i]:
            index_measurement = i
            found_measurement = 1
        elif('Date' in header[i]):
            index_date = i
        elif('Time' in header[i]):
            index_time = i

    if(not found_measurement) :
        print('--- Measurement not found')
        return [-1]

    measurement = []
    time_sample   = []
    for i in range(1,min(len(data)-1, until_point)):
        aux = data[i].split(',')
        measurement.append(float(aux[index_measurement]))
        datetime_i = datetime.datetime.strptime(aux[index_date].lstrip(' ')+' '+aux[index_time].lstrip(' '), '%Y-%m-%d %H:%M:%S.%f')    
        if(i==1) : datetime_initial = datetime_i
        time_sample.append((datetime_i - datetime_initial).total_seconds())
        time_sample[-1] = time_sample[-1]*(1.0/60.0) # normalize to hours

    return [datetime_initial, time_sample, measurement]

# Auxiliar functions for data-processing
def remove_first_value(v):
    v_0 = v[0]
    return [i-v_0 for i in v]	

def remove_mean(v):
    v_0 = np.mean(v)
    return [i-v_0 for i in v]

def scale_factor(v, alpha=1):
    return [i*alpha for i in v]	

def time_scale_f(datetime_sample, time_norm):
    return [(1.0/60.0)*((i-time_norm).total_seconds()) for i in datetime_sample]

def time_removal(time_scale, measurement_scale, tmin, tmax):
    i=0
    time_new        = []
    measurement_new = []	
    while(time_scale[i]<tmin): i = i+1
    while((i+1)<(len(time_scale)+1) and time_scale[i]<=tmax):
        time_new.append(time_scale[i])
        measurement_new.append(measurement_scale[i])
        i=i+1
    return [time_new, measurement_new]

def vector_folding(time_scale, time_scale_ref, vector_ref):
    vector_scale = []
    k=0
    for i in range(0, len(time_scale)):
        while((k+1)<len(time_scale_ref) and (time_scale_ref[k]<time_scale[i])) : k=k+1
        vector_scale.append(vector_ref[k])		
    return vector_scale

def vector_removing(time_scale, vector, times_removal, duration_removal):
    time_scale_new = []
    vector_new     = []
    for i in range(0, len(time_scale)):
        remove = 0
        for j in range(0, len(times_removal)):
            if((time_scale[i]>(times_removal[j]-duration_removal/2)) and
               (time_scale[i]<(times_removal[j]+duration_removal/2))):
                remove = 1
        if(not remove):
            time_scale_new.append(time_scale[i])
            vector_new.append(vector[i])
    return [time_scale_new, vector_new]			


##########  test PLOTS  ###########
def power_test(name_directory='results/hptc_151242626858'):

    data      = read_measurement_power(name_directory + '/power.txt', 'Current(A)')
    timescale = data[1]
    current   = data[2]
    data      = read_measurement_power(name_directory + '/power.txt', 'Voltage(V)')
    voltage   = data[2]

    ###### SCOPE MEASUREMENTS VS. TIME ######
    # Plot
    fig, ax1 = plt.subplots()
    lns_ax1 = []

    lns_ax1.append(ax1.plot(timescale, voltage, color = 'b', marker='o', label='Voltage'))

    lns_ax2 = []
    ax2 = ax1.twinx()
    lns_ax2.append(ax2.plot(timescale, current, color = 'r', marker='o', label='Current'))
    ax2.set_ylabel('Current [A]')
    ax2.yaxis.label.set_color('red')
    ax2.tick_params('y', colors='r')
    ax2.legend(loc = 'lower left')

    # All legends same graph
    ax1.legend(loc='upper left')
    ax1.yaxis.set_ticks_position('left')
    ax1.xaxis.set_ticks_position('both')	
    ax1.yaxis.label.set_color('blue')
    ax1.tick_params('y', colors='b')
    ax1.tick_params(axis='both', direction='in')
    ax1.set_ylabel('Voltage [V]')
    ax1.set_xlabel('Time (minutes)')
    plt.savefig(name_directory + '/img/power.png')
    plt.close()
    
def histogram_plot(name_directory='results/hptc_151242626858'):
    color = ['r','b','g','k']
    
    plt.plot([],[], linewidth=0.0, linestyle=None, label = '|%7s|%7s|%7s|%7s|%7s|%7s|' % ('OUT', 'RMS(ps)', 'AMP(mV)', 'FAL(ps)', 'RIS(ps)', 'Duty(%)'))
    for i in range(0,4):
        [timex, hist, jitter, mean] = read_histogram(name_directory + '/T'+str((i-1)%4)+'/SCOPE_hist', zero_mean=1)
        
        vamp = read_measurement_scope(name_directory + '/T'+str((i-1)%4)+'/SCOPE_measurements.csv', measurement_name='V amptd(2-4)_AVG')
        duty = read_measurement_scope(name_directory + '/T'+str((i-1)%4)+'/SCOPE_measurements.csv', measurement_name='Duty cycle(2-4)_AVG')
        fall = read_measurement_scope(name_directory + '/T'+str((i-1)%4)+'/SCOPE_measurements.csv', measurement_name='Fall time(2-4)_AVG')
        rise = read_measurement_scope(name_directory + '/T'+str((i-1)%4)+'/SCOPE_measurements.csv', measurement_name='Rise time(2-4)_AVG')

        plt.plot(timex, hist, color=color[i], label = ('| %7d  | %7.2f  | %7d   | %7d | %7d | %7.1f |' % (i,jitter, vamp[0]*1e3, fall[0]*1e12, rise[0]*1e12, duty[0]) ))

    plt.ylabel('Probability density function')
    plt.xlabel('Time Interval Error (ps)')
    plt.axis([-10, 10, -0.0005, 0.0065*6/4])
    plt.legend()
    plt.title('Scope measurements')
    plt.savefig(name_directory + '/img/histogram.png')
    plt.close()

def pn_plot(name_directory='results/hptc_151242626858'):

    color = ['r','b','g','k']
    for i in range(0,4):
        [freq, pn, jitter, int_bdw, carrier] = read_phase_noise_fswp(name_directory + '/T'+str((i)%4)+'/PNA_tr1_clock', scale_carrier = 1, new_f_carrier = 40e6, fmin=0, fmax = 999e9)
        plt.semilogx(freq, pn, color=color[i], label = ('OUT%d %6.3fMHz | RMS=%3dfs' % (i, carrier/1e6,jitter*1e3)))

    plt.ylabel('Phase-Noise (dBc) - Fc=40MHz')
    plt.xlabel('Frequency Offset (Hz)')
    plt.axis([1, 10e6, -180,-50])
    plt.legend()
    plt.title('Phase-noise measurements')
    plt.savefig(name_directory + '/img/phase_noise.png')
    plt.close()

def plot_all(name_directory='results/hptc_151242626858'):
    power_test(name_directory)
    histogram_plot(name_directory)
    pn_plot(name_directory)
