#!/usr/bin/env python

import subprocess
import tempfile
import os
import logging
import time

# -------------------------------------------------------------
#  ----------------------- Class PLL_Si534xEVB --------------
# -------------------------------------------------------------

class PLL_Si534xEVB():
    """
        This class was tested with evaluation boards: Si5344 Rev B EVB, Si5345 Rev D EVB
        Commands to the EVB and principle of the class can be found under:
        CLBPro CLI's User Guide
        https://www.silabs.com/documents/public/application-notes/AN922.pdf
        WARNING: This class is OS-dependent (tested on Windows10)
    """

    def __init__(self, device_list_arg = ['00-00-16-CF-74-70'], logger_name=None):
        """
            Class constructor

            args:			
              device_list_arg: accepts a list of strings corresponding to EVB addresses connected to the PC
              logger_name    : string of logger name handling logging
        """		
        self.device_list = device_list_arg
        self.logger = logging.getLogger(logger_name)

    def identify_devices(self):
        """
            Identifies Silicon Labs devices connected to the computer
            Check if default devices are connected and append new devices to the attribute device_list

            return:			
              a boolean list (with same indexing as the attribute device_list) indicating whether the device was identified
        """
        # Launch cmd script	
        detected_devices = subprocess.check_output('CBProDeviceWrite --mode i2c --i2c-address 0x68 --io-voltage 3.3 --list-devices')
        # expected formatting: b'Searching for EVBs/FPs ... \r\n00-00-16-CF-74-70 (Si534xA Rev B EVB)\r\n'		
        detected_devices = detected_devices.decode('utf-8')	
        if(detected_devices.find('No supported Silicon Labs Timing devices') != -1):
            self.logger.warn('identify_devices: No devices connected')
            return []		
        else:
            # Format string
            detected_devices = detected_devices.split('\r')
            detected_devices_formatted = []
            for device in detected_devices[1:(len(detected_devices)-1)]:
                detected_devices_formatted.append((device.split(' ')[0]).split('\n')[1])
            # debug: return detected_devices_formatted

            # Check previously detected devices
            connected_list = []
            for i in range(0,len(self.device_list)):
                device=self.device_list[i]			
                if(device in detected_devices_formatted):
                    connected_list.append(True)
                    self.logger.info('identify_devices: Detected device: ' + device + ' / index: ' + str(i) + ' (previously detected) ')
                else:
                    connected_list.append(False)
                    self.logger.warn('identify_devices: The device     : ' + device + ' which was previously at index: ' + str(i) + ' was not detected')

            # Check new devices
            for device in detected_devices_formatted:
                if(not (device in self.device_list)):
                    connected_list.append(True)
                    self.device_list.append(device)
                    self.logger.info('identify_devices: Detected device: ' + device + ' / index: ' + str(len(self.device_list)-1) + ' (new device) ')       

            return connected_list

    def write_full_config(self, filename='config_Si534x.slabtimeproj', device_index=0):
        """
            Write a configuration file (either a project .slabtimeproj or a register bank .txt) to a device

            args:			
              filename    : project file (.slabtimeproj) or register map(.txt)
              device_index: corresponding index of the desired device in the attribute device_list			  
            return:			
              success 0/1
        """

        # Launch cmd script
        check_extension = filename.split('.')[-1]
        if(check_extension == 'slabtimeproj'):
            #obs: subprocess returns 0 when success, so we invert with a xor
            ret_value = 1 ^ subprocess.call('CBProDeviceWrite --mode i2c --i2c-address 0x68 --io-voltage 3.3 --device ' + self.device_list[device_index] + ' --project ' + filename)
        elif(check_extension == 'txt'):
            ret_value = 1 ^ subprocess.call('CBProDeviceWrite --mode i2c --i2c-address 0x68 --io-voltage 3.3 --device ' + self.device_list[device_index] + ' --registers ' + filename)
        else:
            self.logger.error('write_full_config: Incorrect file type')
            return 0

        if(ret_value) : self.logger.info('write_full_config: Wrote configuration to PLL')
        else          : self.logger.error('write_full_config: Error while writing configuration to PLL')

        return ret_value

    def write_reg(self, reg_bank=[[0xCAFE, 0xFF],[0x1234, 0xAA]], device_index=0):
        """
            Write one or more registers to a device

            args:
              reg_bank    : list of lists containing the register address and values [[reg_addr0, reg_value0],[reg_addr1, reg_value1], ...]			
              device_index: corresponding index of the desired device in the attribute device_list
            return:			
              success 0/1
        """

        # create temp file
        file_descriptor, file_path = tempfile.mkstemp(suffix='.tmp')
        with os.fdopen(file_descriptor, 'w') as open_file:
            for i in range(0,len(reg_bank)):
                open_file.write('0x%04x,0x%02x\n' % (reg_bank[i][0], reg_bank[i][1]) )
        try:
            ret_value = 1 ^ subprocess.call('CBProDeviceWrite --mode i2c --i2c-address 0x68 --io-voltage 3.3 --device ' + self.device_list[device_index] + ' --registers ' + file_path)
        finally:
            os.unlink(file_path)

        if(ret_value) : self.logger.info('write_reg: Wrote registers to PLL')
        else          : self.logger.error('write_reg: Error while writing registers to PLL')

        return ret_value


    def dump_config_to_file(self, filename='dummy.txt', device_index=0):
        """
            Dump configuration of a device to a .txt file ordered by functionality

            args:			
              filename    : text file to save the configuration (.txt)
              device_index: corresponding index of the desired device in the attribute device_list			  
            return:			
              success 0/1
        """
        ret_value = 1 ^ subprocess.call('CBProDeviceRead --mode i2c --i2c-address 0x68 --io-voltage 3.3 --device ' + self.device_list[device_index] + ' --all --outfile ' + filename)

        if(ret_value) : self.logger.info('dump_config_to_file: Dumped config to file')
        else          : self.logger.error('dump_config_to_file: Error while dumping config to file')

        return ret_value

    def read_reg(self, reg_addr=[0xCAFE, 0x1234], device_index=0):
        """
            Read one or more registers of a device

            args:			
              reg_addr    : list containing the registers to be read [reg_addr0, reg_addr1, ...]
              device_index: corresponding index of the desired device in the attribute device_list			  
            return:			
              [success 0/1, reg_value0, reg_value1, ...]
        """
        registers_format = ''
        for i in reg_addr: registers_format = registers_format + ' ' + ('0x%04x' % i)
        try:
            regbank=subprocess.check_output('CBProDeviceRead --mode i2c --i2c-address 0x68 --io-voltage 3.3 --device ' + self.device_list[device_index] + ' --registers' + registers_format)
        except subprocess.CalledProcessError:
            self.logger.error('read_reg: CBProDeviceRead --mode i2c --i2c-address 0x68 --io-voltage 3.3 did not execute properly, check the device address with identify_devices()')
            return [0]

        regbank = (regbank.decode('utf-8')).split('\r')
        regbank_formatted = [1]
        for i in range(2,len(regbank)-1):
            aux = list(filter(None,regbank[i].split(' ')))		
            if(int(aux[0],16) == reg_addr[i-2]): #Addr checking
                regbank_formatted.append(int(aux[2],16)) #Value           				
            else:
                self.logger.error('read_reg: Address error when reading from device ' + self.device_list[device_index] + ' - exp:' + reg_addr[i-2] + ' / rcvd:' + str(int(aux[0],16)))
                return [0]

        return regbank_formatted

    def debug_raw_command(self, command='CBProDeviceWrite --mode i2c --i2c-address 0x68 --io-voltage 3.3 --help'):
        """
            Raw command send to system console in order to debug device
            Only to help in debug purposes, scripts using this class should not use this method
            args:			
              command    : command as a string		  
            return:			
              success 0/1
        """
        return 1 ^ subprocess.call(command)        

    def check_pll_locked(self, time_wait_max=10, device_index=0):
        """
            Check if PLL is locked with a timeout
            args:			
              time_wait_max : timeout for wait lock in seconds
              device_index  : corresponding index of the desired device in the attribute device_list					  
            return:			
              success 0/1: unlocked/locked
        """
        lol = 1
        t0 = time.time()
        while(lol):
            status = self.read_reg([0x000E],device_index)
            if(not status[0]):
                self.logger.warn('check_pll_locked: Communication with PLL failed')
                return 0
            else:
                lol = (status[1] >> 1) & 0x1

            t1 = time.time()
            if(((t1-t0) > time_wait_max) and lol):
                self.logger.warn('check_pll_locked: TIMEOUT reached, PLL NOT LOCKED')
                return 0
        self.logger.info('check_pll_locked: PLL LOCKED')
        return 1

    def clear_sticky(self,device_index=0):
        """
            Write one or more registers to a device

            args:
              device_index: corresponding index of the desired device in the attribute device_list
            return:			
              success 0/1
        """

        # clear status
        status = self.write_reg([[0x0012,0x00],[0x0013,0x00]],device_index)
        return status

    def check_sticky(self, device_index=0):
        """
            Check if PLL is locked with a timeout
            args:			
              device_index  : corresponding index of the desired device in the attribute device_list					  
            return:			
              [success, LOL_FLG, LOS_FLG, OOF_FLG]
        """
        status = self.read_reg([0x0012],device_index)
        if(not status[0]):
            self.logger.warn('check_pll_locked: Communication with PLL failed')
            return [0]
        else:
            LOS_FLG = (status[1] >> 0) & 0xF
            OOF_FLG = (status[1] >> 4) & 0xF

        status = self.read_reg([0x0013],device_index)
        if(not status[0]):
            self.logger.warn('check_pll_locked: Communication with PLL failed')
            return [0]
        else:
            LOL_FLG = (status[1] >> 1) & 0x1


        self.logger.info('check_pll_locked: PLL LOCKED')
        return [1,LOL_FLG,LOS_FLG,OOF_FLG]
