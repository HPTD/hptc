#!/usr/bin/env python

from . visa_comm import VisaComm
import time


# -----------------------------------------------------------------------------------------
#  ----------------------- Class Agilent Lightwave Multimeter 8163B -----------------------
# -----------------------------------------------------------------------------------------


class multimeter(VisaComm):

	""" 
		All the commands to this instrument can be found under: https://literature.cdn.keysight.com/litweb/pdf/08164-90B65.pdf?id=118032 
		WARNING: After the first connection, if the device has to be turned off, it is fundamental to reconnect the LAN cable to the 
		instrument only after its initial calibration is concluded, otherwise the device will not be detected
	"""

	
	def __init__(self, rm_visa, str_addr, logger_name):
		"""
            Class constructor

            args:		
				rm_visa: Visa Resource Manager
				str_addr: a string containing the IP address of the instrument
                logger_name : string of logger name handling logging						 
        """		
		VisaComm.__init__(self, rm_visa, str_addr, logger_name)
	
	def get_id(self):
		"""
			Ensure to identify the instrument
			
			return:
				a string containing the manufacturer, instrument model number, serial number, and firmware revision level
		"""
		
		stat = self.query('*IDN?')
		return stat
	
	
	def get_pwr_unit(self):
		""" 
			Get the sensor power unit.
			N.B The instruction is referred to the sensor 2 and the channel 1 (the ones used as default) 
		
			return:
				a string with the power units, i.e. 'dBm' or 'watt'
		"""
		
		pwr_unit=self.query('SENS2:CHAN1:POW:UNIT?')
		pwr_un=float(pwr_unit)
		if (pwr_un!=0):
			pwr_response='watt'
		else:	
			pwr_response='dBm'
		return pwr_response	

		
	def set_pwr_unit(self,value):
		""" 
			Set the sensor power unit.
			N.B The instruction is referred to the sensor 2 and the channel 1 (the ones used as default) 
		
			arg:
				value: an integer value chosen on the base of these equivalences, i.e. 0 = dBm, 1 = Watt

		"""
		
		self.send('SENS2:CHAN1:POW:UNIT'+' '+str(value))


	def set_wave_length(self,value):
		""" 
			Set the sensor wave length.
			N.B The instruction is referred to the sensor 2 and the channel 1 (the ones used as default) 
		
			arg:
				value: a float value expressed in nanometers or one of the following terms, i.e.
					   1) MIN, inteded as the minimum programmable value
					   2) MAX, inteded as the maximum programmable value
					   3) DEF, intended as half the sum of the minimum programmable value and the maximum programmable value

		"""

		self.send('SENS2:CHAN1:POW:WAV '+str(value)+'nm')
	
	def get_power(self):
		"""
			Get the current power meter value
			
			return:
				 the power as a float value in dBm or W, basing on the power unit previously set
		"""
		power=self.query('fetc2:pow?')	
		return float(power)
	
	#Zeroing all powermeter channels
	def zeroing(self):
		"""
			Zeros the electrical offset for the power meter
		"""	
		
		self.send('OUTP1:CORR:COLL:ZERO:ALL')
		time.sleep(24)


	