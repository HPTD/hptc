#!/usr/bin/env python

import time
import logging
import serial

# -------------------------------------------------------------
#  --------------------- Class SerialComm -------------------
# -------------------------------------------------------------

class SerialComm:

    def __init__(self, logger_name=None, port_address='COM22', baudrate=9600, parity='N', stopbits=1, bytesize=8, timeout=10000):
        self.logger = logging.getLogger(logger_name)
        self.ser = serial.Serial(port = port_address, baudrate = baudrate, parity=parity, stopbits=stopbits, bytesize = bytesize)
        if(self.ser.isOpen()):
            self.logger.info('Successfuly connected to serial port ' + port_address)		
        else:
            self.logger.warn('Problem in connection to serial port ' + port_address + ' - check your device is properly connected')	
        self.TIMEOUT = timeout

    def __del__(self):
        self.ser.close()

    def send(self, msg):
        return self.ser.write(bytes(msg + '\r\n', 'utf-8'))

    def query(self, msg):
        self.send(msg)
        out = ''
        t0 = time.time()
        tdelta = 0
        while(self.ser.inWaiting() == 0 and tdelta<self.TIMEOUT/1e3):
            tdelta = time.time() - t0
        while(self.ser.inWaiting() > 0):
            out += (self.ser.read(1)).decode('utf-8')
        
        return out
