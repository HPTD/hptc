#!/usr/bin/env python

import math
import time
import datetime
from . serial_comm import SerialComm

# -------------------------------------------------------------
#  --------------- Class FunctionGenerator -------------------
# -------------------------------------------------------------

class FunctionGenerator(SerialComm):

    """
        Class to control Function Generator (TG5011A) from KeySight
        Commands to this device can be found under: 
        http://resources.aimtti.com/manuals/TG5012A_2512A_5011A+2511A_Instructions-Iss4.pdf
    """

    def __init__(self, logger_name=None, port_address='COM22', baudrate=9600, parity='N', stopbits=1, bytesize=8, timeout = 10000):
        """
            Class constructor

            args:			
              rm_visa     : VISA resource manager (opened with rm_visa = visa.ResourceManager())
              str_addr    : VISA address, can be checked through Connection Expert application
                            for more information https://www.keysight.com/upload/cmc_upload/All/readme_IOLS_16_3_16603_3.htm?&cc=CN&lc=chi
              logger_name : string of logger name handling logging						 
        """		
        SerialComm.__init__(self, logger_name, port_address, baudrate, parity, stopbits, bytesize, timeout)

    def get_id(self):
        """
            Get instrument ID

            return:
                instrument ID (check VISA reference for more information)
        """
        stat = self.query('*IDN?')
        return stat

    def cls(self):
        """
            Clear Status registers

            return:
                1 when instrument is operational
        """
        stat = self.send('*CLS')
        return int(self.query('*OPC?'))

    def preset(self):
        """
            Resets instrument parameters to their default values
        """
        stat = self.send('*RST')

    def enable_output(self):
        """
            Enable output				   
        """
        stat = self.send('OUTPUT ON')

    def disable_output(self):
        """
            Disable output				   
        """
        stat = self.send('OUTPUT OFF')

    def set_wave_type(self, wave_str = 'NOISE'):
        """
            Set wave type

            args:
                wave_str : possible values are 'SINE','SQUARE','RAMP','TRIANG','PULSE','NOISE',
                                               'PRBSPN7','PRBSPN9','PRBSPN11','PRBSPN15','PRBSPN20',
                                               'PRBSPN23' or 'ARB'											   
        """
        stat = self.send('WAVE ' + wave_str)

    def set_wave_freq(self, freq=10e3):
        """
            Set wave frequency

            args:
                freq : frequency in Hz							   
        """
        stat = self.send('FREQ ' + str(freq))

    def set_wave_ampunit(self, ampunit = 'VRMS'):
        """
            Set wave amplitude unit
            It is recommended to run this command prior to the execution of set_wave_amp(amplitude)

            args:
                ampunit : possible values are 'VPP', 'VRMS' or 'DBM'										   
        """
        stat = self.send('AMPUNIT ' + ampunit)

    def set_wave_ampl(self, ampl=0.1):
        """
            Set wave amplitude in the units as specified by the set_wave_ampunit(ampunit) command

            args:
                ampl : amplitude in V						   
        """
        stat = self.send('AMPL ' + str(ampl))

    def set_wave_dc_offset(self, dc_offset=0.0):
        """
            Set wave offset in V

            args:
                dc_offset : DC offset in volts						   
        """
        stat = self.send('DCOFFS ' + str(dc_offset))