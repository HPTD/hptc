#!/usr/bin/env python

import math
import time
import visa
import datetime
from . visa_comm import VisaComm

# -------------------------------------------------------------
#  ------------- Class PhaseNoiseAnalyzer ----------------
# -------------------------------------------------------------

class PhaseNoiseAnalyzer(VisaComm):

    """
        Class to control Phase Noise Analyzer (FSWP) from Rohde&Schwarz	
        Commands to this device can be found under: 
        https://cdn.rohde-schwarz.com/pws/dl_downloads/dl_common_library/dl_manuals/gb_1/f/fswp_1/FSWP_UserManual_en_09.pdf
    """

    def __init__(self, rm_visa, str_addr, logger_name):
        """
            Class constructor

            args:			
              rm_visa     : VISA resource manager (opened with rm_visa = visa.ResourceManager())
              str_addr    : VISA address, can be checked through Connection Expert application
                            for more information https://www.keysight.com/upload/cmc_upload/All/readme_IOLS_16_3_16603_3.htm?&cc=CN&lc=chi
              logger_name : string of logger name handling logging						 
        """		
        VisaComm.__init__(self, rm_visa, str_addr, logger_name)

    
    def get_id(self):
        """
            Get instrument ID

            return:
                instrument ID (check VISA reference for more information)
        """
        stat = self.query('*IDN?')
        return stat

    
    def cls(self):
        """
            Clear Status registers

            return:
                1 when instrument is operational
        """
        stat = self.send('*CLS')
        return int(self.query('*OPC?'))

    
    def preset(self):
        """
            Preset instrument and clear ctatus registers (Preset -> Factory)
        """
        stat = self.send(':SYST:PRES')
        self.cls()

    
    def set_phase_noise_measurement(self):
        """
            Activate phase-noise measurement (Meas -> Select Meas -> Phase Noise)
			
            return:
                n_corr : number of correlation points
        """
        self.send(':CONF:PNO:MEAS PNO')

    
    def set_offset(self, start_offset=1e3, stop_offset=10e6):
        """
            Set start and stop offset for phase-noise measurement (Meas Config -> Noise Config -> Start/Stop Offset)

            args:
                start_offset: frequency in Hz (values are rounded to the closest valid by the instrument)
                stop_offset: frequency in Hz (values are rounded to the closest valid by the instrument)
        """
        self.send(':SENS:FREQ:STAR ' + str(start_offset))
        self.send(':SENS:FREQ:STOP ' + str(stop_offset))

    
    def get_offset(self):
        """
            Get start and stop offset for phase-noise measurement settings (Meas Config -> Noise Config -> Start/Stop Offset)

            return:
                [start_offset, stop_offset]
                start_offset: frequency in Hz 
                stop_offset: frequency in Hz
        """
        start_offset = float(self.query(':SENS:FREQ:STAR?'))
        stop_offset  = float(self.query(':SENS:FREQ:STOP?'))
        return [start_offset, stop_offset]

    
    def set_bdw_resolution(self, bdw):
        """
            Set resolution bandwidth applied to each half decade (BW -> Res BW)

            args:
                bdw : resolution bandwidth in percentage
        """
        self.send(':SENS:LIST:BWID:RAT ' + str(bdw))

    
    def get_bdw_resolution(self):
        """
            Get resolution bandwidth applied to each half decade (BW -> Res BW)

            return:
                bdw : resolution bandwidth in percentage
        """
        bdw = float(self.query(':SENS:LIST:BWID:RAT?'))
        return bdw

    
    def set_automatic_frequency_search(self, flimit_low=1e6, flimit_high=8e9, plevel_threshold=-20, capture_range='NORM'):
        """
            Turns on and Set Instrument Automatic Frequency Search (Freq Channel -> Auto Search/Auto Search Config)

            args:
                flimit_low       : lower limit for frequency search in Hz
                flimit_high      : upper limit for frequency search in Hz
                plevel_threshold : power level threshold for signal search in dBm				
                capture_range    : 'WIDE' - for DUTs whose frequency drifts quickly over big distances
                                'NORM' - for stable DUTs or DUTs whose frequency drifts slowly over a short distance	
        """
        self.send(':SENS:ADJ:CONF:FREQ:AUT:STAT 1')
        self.send(':SENS:ADJ:CONF:FREQ:LIM:LOW ' + str(flimit_low))
        self.send(':SENS:ADJ:CONF:FREQ:LIM:HIGH ' + str(flimit_high))
        self.send(':SENS:ADJ:CONF:LEV:THR ' + str(plevel_threshold))
        self.send(':SENS:SWE:CAPT:RANG ' + capture_range)

            
    def set_trace(self, trace_nbr=1, mode='WRIT', label_on=0, label_str='clock', result='PN',
                  smoothing_en=1, smoothing_pct=1, spur_remove_en=0, spur_remove_thr=6):
        """
            Set Instrument Trace Characteristics (TRACE)

            args:
                trace_nbr : 1,2,3,4,5,6			
                mode : trace mode, it can have the following values
                     'WRIT' = trace is overwritten by each sweep (default)
                     'AVER' = trace is formed over several sweeps (the function set_averaging determines the number of averaging)
                     'MAX'  = maximum value is determined over several sweeps and displayed
                     'MIN'  = minimum value is determined over several sweeps and displayed
                     'VIEW' = current contents of the trace memory are frozen and displayed
                     'BLAN' = hides the selected trace
                     'WRH'  = trace is overwritten when new data is available but only after all cross-correlation operations defined for a half decade are done
                
                label_on         : display label on trace (1/0)
                label_str        : text to be displayed on trace
                result           : 'PN', 'AM' or 'PNAM'
                smoothing_en     : enables trace smoothing				
                smoothing_pct    : smoothing aperture in percentage				
                spur_remove_en   : remove spurs from trace
                spur_remove_thr  : spur detection level in dB 				
        """
        self.send(':DISP:TRAC' + str(trace_nbr) + ':MODE ' + mode)
        self.send(':DISP:TRAC' + str(trace_nbr) + ':LAB:STAT ' + str(label_on))
        if(label_on) : self.send(':DISP:TRAC' + str(trace_nbr) + ':LAB:TEXT "' + label_str+'"')
        self.send(':DISP:TRAC' + str(trace_nbr) + ':RES ' + result)
        self.send(':DISP:TRAC' + str(trace_nbr) + ':SMO:STAT ' + str(smoothing_en))
        if(smoothing_en) : self.send(':DISP:TRAC' + str(trace_nbr) + ':SMO:APER ' + str(smoothing_pct))
        self.send(':DISP:TRAC' + str(trace_nbr) + ':SPUR:SUPP ' + str(spur_remove_en))
        if(spur_remove_en) : self.send(':DISP:TRAC' + str(trace_nbr) + ':SPUR:THR ' + str(spur_remove_thr))

    
    def set_averaging(self, n_avg=4):
        """
            Set averaging number (Meas Config -> Noise Config -> Sweep/Avg Count)

            args:
                n_avg : number of averaging measurements
        """
        self.send(':SENS:SWE:COUN ' + str(n_avg))

    
    def get_averaging(self):
        """
            Get averaging number (Meas Config -> Noise Config -> Sweep/Avg Count)

            return:
                n_avg : number of averaging measurements
        """
        n_avg  = float(self.query(':SENS:SWE:COUN?'))
        return n_avg

    
    def set_correlation(self, n_corr=1):
        """
            Set correlation number (Meas Config -> XCORR Factor)

            args:
                n_corr: number correlation points
        """
        self.send(':SENS:SWE:XFAC ' + str(n_corr))

    
    def get_correlation(self):
        """
            Get number of correlations setting (Meas Config -> XCORR Factor)
			
            return:
                n_corr : number of correlation points
        """
        n_corr = float(self.query(':SENS:SWE:XFAC?'))
        return n_corr

    
    def get_carrier(self):
        """
            Get carrier characteristics (frequency and power)
			
            return:
                [f_carrier, p_carrier]: f_carrier is the carrier frequency in Hz and p_carrier is the carrier power in dBm
        """
        frequency = float(self.query(':SENS:FREQ:CENT?'))		
        power = float(self.query(':SENS:POW:RLEV?'))		
        return [frequency, power]

    
    def set_pn_analysis_range(self, range_nbr=1, trace_nbr=1, custom_range=0, f_start=1, f_stop=10e6):
        """
            Set phase noise analysis range

            args:
                range_nbr    : 1,2,3,4,5,6,7,8,9,10
                trace_nbr    : 1,2,3,4,5,6			
                custom_range : 1 = custom range, 0 = full measurement range
                f_start      : start frequency for custom range in Hz
                f_stop       : stop frequency for custom range in Hz
       
        """
        self.send(':CALC:RANG' + str(range_nbr) + ':EVAL:TRAC TRACE' + str(trace_nbr))
        if(custom_range) : str_on_off = 'OFF'
        else             : str_on_off = 'ON'
        self.send(':CALC:RANG' + str(range_nbr) + ':EVAL ' + str_on_off)
        if(custom_range):
            self.send(':CALC:RANG' + str(range_nbr) + ':EVAL:STAR ' + str(f_start))
            self.send(':CALC:RANG' + str(range_nbr) + ':EVAL:STOP ' + str(f_stop))

    
    def get_pn_analysis_data(self, range_nbr=1, save_to_file=0, filename='dummy.csv'):
        """
            Get phase noise analysis data

            args:
                range_nbr    : 1,2,3,4,5,6,7,8,9,10

            return:
                [integrated_phase_noise, rms_jitter, f_start, f_stop, residual_pm, residual_fm, residual_am]
                integrated_phase_noise : dBc 
                f_start                : integration range start in Hz
                f_stop                 : integration range stop in Hz					
                rms_jitter             : seconds
                residual_pm            : degrees
                residual_fm            : hertz
                residual_am            : percentage
        """
        integrated_phase_noise = float(self.query(':FETC:RANG' + str(range_nbr) + ':PNO:IPN?'))
        rms_jitter             = float(self.query(':FETC:RANG' + str(range_nbr) + ':PNO:RMS?'))
        f_start                = float(self.query(':CALC:RANG' + str(range_nbr) + ':EVAL:STAR?'))
        f_stop                 = float(self.query(':CALC:RANG' + str(range_nbr) + ':EVAL:STOP?'))		
        residual_pm            = float(self.query(':FETC:RANG' + str(range_nbr) + ':PNO:RPM?'))
        residual_fm            = float(self.query(':FETC:RANG' + str(range_nbr) + ':PNO:RFM?'))
        residual_am            = float(self.query(':FETC:RANG' + str(range_nbr) + ':PNO:RAM?'))

        if(save_to_file):
            with open(filename, 'w') as f:
                f.write('%30s,%20.4f\n' % ('Integrated Phase Noise (dBc)', integrated_phase_noise))
                f.write('%30s,%20.4f\n' % ('RMS Jitter (ps)', rms_jitter*1e12))
                f.write('%30s,%20.4f\n' % ('Frequency Start (Hz)', f_start))
                f.write('%30s,%20.4f\n' % ('Frequency Stop (Hz)', f_stop))
                f.write('%30s,%20.4f\n' % ('Residual PM (deg)', residual_pm))
                f.write('%30s,%20.4f\n' % ('Residual FM (deg)', residual_fm))
                f.write('%30s,%20.4f\n' % ('Residual AM (deg)', residual_am))				
        return [integrated_phase_noise, rms_jitter, f_start, f_stop, residual_pm, residual_fm, residual_am]

    
    def get_phase_noise_data(self, trace_nbr=1):
        """
            Get frequency and phase-noise data

            args:
              trace_nbr = 1-6

            return:
              [freq, phase_noise]
              freq = [f_0, f_1, f_2, ...]: f_i is the frequency for point i in Hz
              phase_noise = [pn_0, pn_1, pn_2, ...]: pn_i is the phase-noise for point i in dBc/Hz
        """

        data        = self.query(':TRAC:DATA? TRACE' + str(trace_nbr))
        data        = data.split(',')
        freq        = [float(data[i]) for i in range(0,len(data),2)]
        phase_noise = [float(data[i]) for i in range(1,len(data),2)]
        return [freq, phase_noise]

    
    def config_average_trigger(self):
        """
            Configure for average trigger
        """
        self.send(':INIT:CONT OFF')
        self.abort_measurement()

    
    def average_trigger(self):
        """
            Run average trigger
        """
        self.send(':INIT:IMM')

    
    def check_busy(self):
        """
            Check if the instrument is busy

            return:
              0/1: measurement is finished/running
        """
        try:
            finished = int(self.query('*OPC?'))
            return 1^finished
        except visa.VisaIOError:
            return 1

    
    def run_measurement_routine(self):
        """
            Main instrument routine run with average trigger

            return:
              [1, '']: measurement finished with no errors
              [0, 'ERROR_NAME0; ERROR_NAME1; ...;']: error happened
        """
        run_measurement = 1
        while(run_measurement):
            self.config_average_trigger()

            # Clear status registers
            self.cls()

            # Enable trigger and wait for measurement to finish
            self.average_trigger()
            wait_measurement = True
            while(wait_measurement):
                measurement_running      = self.check_busy()
                wait_measurement = measurement_running
                time.sleep(0.1)
            measurement_error_status = self.questionable_error_handling(1,1)
            # Check error status of measurement
            if(measurement_error_status[0]):
                run_measurement = 0
                self.logger.info('run_measurement_routine: Measurement finished with no errors')
            # Error occurred and do not know automatically how to adjust
            else: 
                run_measurement = 0
                self.abort_measurement()
                self.logger.error('run_measurement_routine: Measurement error - ' + measurement_error_status[1])

        return measurement_error_status

    
    def abort_measurement(self):
        """
            Aborts measurement
        """
        self.send(':ABOR')

    
    def questionable_error_handling(self, read_global_quest=0, read_status_quest=0):
        """
            Identifies Questionable Error in the instrument - more details are available on Appendix B of programming manual
            OBS: not all the errors conditions were implemented yet in this function

            args: 
              read_global_quest: (0/1) Read Global Questionable Status Event Register? (obs: prior enabling needed)
              read_status_quest: (0/1) Read Questionable Status Event Register?        (obs: prior enabling needed)

            return:
              [1, '']: no errors identified
              [0, 'ERROR_NAME0; ERROR_NAME1; ...;']: error happened
        """

        error_vector = [1]
        error_vector.append('')

        # Read Global Questionable Status Event Register
        if(read_global_quest):
            global_questionable       = int(self.query(':STAT:QUES?'))
            STAT_QUES_INTERRUPT       = (global_questionable >> 0)  & 0x0001

            if(STAT_QUES_INTERRUPT):
                error_vector[0] = 0
                error_vector[1] = error_vector[1] + ('STAT_QUES_INTERRUPT;')

        if(read_status_quest):
            # Read All Questionable Status Event Register
            status_questionable       = int(self.query(':STAT:QUES:COND?'))
            TIME_SUMMARY              = (status_questionable >> 2)  & 0x0001
            POWER_SUMMARY             = (status_questionable >> 3)  & 0x0001
            TEMPERATURE_SUMMARY       = (status_questionable >> 4)  & 0x0001
            FREQUENCY_SUMMARY         = (status_questionable >> 5)  & 0x0001
            CALIBRATION_SUMMARY       = (status_questionable >> 8)  & 0x0001
            LIMIT_SUMMARY             = (status_questionable >> 9)  & 0x0001
            LMARGIN_SUMMARY           = (status_questionable >> 10)  & 0x0001
            ACPLIMIT_SUMMARY          = (status_questionable >> 12)  & 0x0001
            PNOISE_SUMMARY            = (status_questionable >> 13)  & 0x0001

            if(TIME_SUMMARY):
                error_vector[0] = 0
                error_vector[1] = error_vector[1] + ('TIME_SUMMARY;')
            if(POWER_SUMMARY):
                error_vector[0] = 0
                error_vector[1] = error_vector[1] + ('POWER_SUMMARY;')
            if(TEMPERATURE_SUMMARY):
                error_vector[0] = 0
                error_vector[1] = error_vector[1] + ('TEMPERATURE_SUMMARY;')
            if(FREQUENCY_SUMMARY):
                error_vector[0] = 0
                error_vector[1] = error_vector[1] + ('FREQUENCY_SUMMARY;')
            if(CALIBRATION_SUMMARY):
                error_vector[0] = 0
                error_vector[1] = error_vector[1] + ('CALIBRATION_SUMMARY;')
            if(LIMIT_SUMMARY):
                error_vector[0] = 0
                error_vector[1] = error_vector[1] + ('LIMIT_SUMMARY;')
            if(LMARGIN_SUMMARY):
                error_vector[0] = 0
                error_vector[1] = error_vector[1] + ('LMARGIN_SUMMARY;')
            if(ACPLIMIT_SUMMARY):
                error_vector[0] = 0
                error_vector[1] = error_vector[1] + ('ACPLIMIT_SUMMARY;')
            if(PNOISE_SUMMARY):
                error_vector[0] = 0
                error_vector[1] = error_vector[1] + ('PNOISE_SUMMARY;')


        return error_vector

    
    def save_trace(self, trace_nbr=1, filename='C:/test.csv', spurfilename='C:/spurs_test.csv'):
        """
            Save trace of instrument and spurs (spursfilename)

            args:
              trace_nbr = 1-6 trace to store			
              filename  = string for filename with trace in .csv format
              filename  = string for filename with spurs .csv format
        """
        # save trace
        self.send(':MMEM:STOR:TRAC ' + str(trace_nbr) + ', "C:/dummy.csv"')
        data = self.query(':MMEM:DATA? "C:/dummy.csv"')
        with open(filename, 'w') as f:
            f.write(data)			
        self.delete_file_local('C:/dummy.csv')

        # save spurs
        self.send(':MMEM:STOR:SPUR "C:/dummy.csv",' + str(trace_nbr))
        data = self.query(':MMEM:DATA? "C:/dummy.csv"')
        with open(spurfilename, 'w') as f:
            f.write(data)			
        self.delete_file_local('C:/dummy.csv')

    
    def save_screen(self, filename='C:/dummy.png'):
        """
            Save screen image

            args:
              filename = string for filename with .png format
        """
        self.send(':MMEM:NAME "C:/dummy.png"')
        self.send(':HCOP:DEST1 "SYST:COMM:CLIP"')
        self.send(':HCOP:CONT WIND')
        self.send(':HCOP:DEV:LANG1 PNG')
        self.send(':HCOP:IMM')
        data = self.query_binary_values(':MMEM:DATA? "C:/dummy.png"', datatype='B', is_big_endian=True, container=bytearray)
        with open(filename, 'wb') as f:
            f.write(data)
        self.delete_file_local('C:/dummy.png')

    
    def delete_file_local(self, filename):
        """
            Delete file from the instruments local hard-disk

            args:
             filename = string as a filename
        """
        self.send(':MMEM:DEL "' + filename + '"')
