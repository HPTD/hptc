#!/usr/bin/env python

import time
import logging

# -------------------------------------------------------------
#  ----------------------- Class VisaComm --------------------
# -------------------------------------------------------------

class VisaComm:

    def __init__(self, rm_visa, str_addr, logger_name=None, timeout=10000):
        self.remote_ctrl = rm_visa.open_resource(str_addr)
        self.remote_ctrl.timeout = timeout
        self.remote_ctrl.clear()
        self.logger = logging.getLogger(logger_name)

    def __del__(self):
        del self.remote_ctrl

    def send(self, msg):
        return self.remote_ctrl.write(msg)

    def query(self, msg):
        return self.remote_ctrl.query(msg)

    def query_binary_values(self, msg, datatype, is_big_endian, container):
        return self.remote_ctrl.query_binary_values(msg, datatype, is_big_endian, container)

    def wait_for_opc(self):
        self.send('*CLS')
        self.send('*OPC')
        esr_value = 0
        while(esr_value&1 == 0):
            time.sleep(0.1)		
            esr_value = int(self.query('*ESR?'))
