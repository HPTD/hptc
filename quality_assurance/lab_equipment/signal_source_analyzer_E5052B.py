#!/usr/bin/env python

import math
import time
import datetime
from . visa_comm import VisaComm

# -------------------------------------------------------------
#  ------------- Class SignalSourceAnalyzer ----------------
# -------------------------------------------------------------

class SignalSourceAnalyzer(VisaComm):

    """
        Class to control Signal Source Analyzer (E5052) from KeySight	
        Commands to this device can be found under: 
        https://literature.cdn.keysight.com/litweb/pdf/E5052-90041.pdf?id=498591
    """

    def __init__(self, rm_visa, str_addr, logger_name):
        """
            Class constructor

            args:			
              rm_visa     : VISA resource manager (opened with rm_visa = visa.ResourceManager())
              str_addr    : VISA address, can be checked through Connection Expert application
                            for more information https://www.keysight.com/upload/cmc_upload/All/readme_IOLS_16_3_16603_3.htm?&cc=CN&lc=chi
              logger_name : string of logger name handling logging						 
        """		
        VisaComm.__init__(self, rm_visa, str_addr, logger_name)

    def get_id(self):
        """
            Get instrument ID

            return:
                instrument ID (check VISA reference for more information)
        """
        stat = self.query('*IDN?')
        return stat

    def cls(self):
        """
            Clear Status registers

            return:
                1 when instrument is operational
        """
        stat = self.send('*CLS')
        return int(self.query('*OPC?'))

    def preset(self):
        """
            Preset instrument and clear ctatus registers (Preset -> Factory)
        """
        stat = self.send(':SYST:PRES')
        self.cls()

    def maximize_display(self, value=1):
        """
            Maximize display (Window Max)
        """
        stat = self.send(':DISP:MAX ' + str(value))

    def set_offset(self, start_offset=1e3, stop_offset=10e6):
        """
            Set start and stop offset for phase-noise measurement (Start/Center and Stop/Span)

            args:
                start_offset: frequency in Hz (valid values: 1, 10, 100, 1e3)
                stop_offset: frequency in Hz (valid values: 100e3, 1e6, 5e6, 10e6, 20e6, 40e6, 100e6 - depending on frequency band)
        """
        self.send(':SENS:PN1:FREQ:STAR ' + str(start_offset))
        self.send(':SENS:PN1:FREQ:STOP ' + str(stop_offset))

    def get_offset(self):
        """
            Get start and stop offset for phase-noise measurement settings (Start/Center and Stop/Span)

            return:
                [start_offset, stop_offset]
                start_offset: frequency in Hz 
                stop_offset: frequency in Hz
        """
        start_offset = float(self.query(':SENS:PN1:FREQ:STAR?'))
        stop_offset  = float(self.query(':SENS:PN1:FREQ:STOP?'))
        return [start_offset, stop_offset]

    def set_if_gain(self, if_gain=20):
        """
            Set IF Gain parameter (Setup -> IF Gain)

            args:
                if_gain: IF gain in dB (valid values: 0, 10, 20, 30, 40, 50)
        """
        self.send(':SENS:PN1:IFG ' + str(if_gain))

    def get_if_gain(self):
        """
            Get IF Gain parameter setting (Setup -> IF Gain)

            return:
                if_gain: IF gain in dB
        """
        if_gain = float(self.query(':SENS:PN1:IFG?'))
        return if_gain

    def set_fband(self, fband=4):
        """
            Set Instrument Frequency Band (Setup -> Frequency Band)

            args:
                fband: frequency band valids are 1 (10M-41MHz), 2 (39M-101MHz), 3 (99M-1.5GHz), 4 (250M-7GHz)
        """
        self.send(':SENS:PN1:FBAND BAND' + str(fband))

    def get_fband(self):
        """
            Get Instrument Frequency Band setting (Setup -> Frequency Band)

            return:
                fband: frequency band can be 1 (10M-41MHz), 2 (39M-101MHz), 3 (99M-1.5GHz), 4 (250M-7GHz)
        """
        fband = float(((self.query(':SENS:PN1:FBAND?')).split('BAND'))[1])
        return fband

    def set_attenuation(self, att_level=5):
        """
            Set Instrument input attenuation (Attn -> Input Attenuator)

            args:
                att_level: attenuation level in dB (valid values: 0 to 35dB in 5dB steps)
        """
        self.send(':SENS:ATT:LEV ' + str(att_level))

    def set_pn_analysis_range_full(self):
        """
            Set phase noise analysis range to full range
        """
        self.send(':CALC:PN1:TRAC1:FUNC:DOM:X FRAN')
        self.send(':CALC:PN1:TRAC1:FUNC:DOM:Y FRAN')

    def set_pn_analysis_integral(self):
        """
            Displays integral phase noise analysis on instruments screen
        """
        self.send(':CALC:PN1:TRAC1:FUNC:TYPE INT')

    def get_attenuation(self):
        """
            Get Instrument input attenuation setting (Attn -> Input Attenuator)

            return:
                att_level: attenuation level in dB
        """
        att_level = float(self.query(':SENS:ATT:LEV?'))
        return att_level

    def clear_averaging(self):
        """
            Clear averaging (Avg/BW -> Averaging Restart)
        """
        self.send(':SENS:PN1:AVER:CLE')

    def set_averaging(self, n_avg=16, en_avg=0):
        """
            Set averaging number and enabling (Avg/BW -> Avg Factor / Avg/BW -> Averaging ON/OFF)

            args:
                n_avg : number of averaging points
                en_avg: enable averaging
        """
        self.send(':SENS:PN1:AVER:COUN ' + str(n_avg))
        self.send(':SENS:PN1:AVER:STAT ' + str(en_avg))

    def get_averaging(self):
        """
            Get averaging number and enabling settings (Avg/BW -> Avg Factor / Avg/BW -> Averaging ON/OFF)

            return:
                [n_avg, en_avg]
                n_avg : number of averaging points
                en_avg: enable averaging
        """
        n_avg  = float(self.query(':SENS:PN1:AVER:COUN?'))
        en_avg = float(self.query(':SENS:PN1:AVER:STAT?'))
        return [n_avg, en_avg]

    def set_correlation(self, n_corr=1):
        """
            Set correlation number (Avg/BW -> Correlation)

            args:
                n_corr: number correlation points
        """
        self.send(':SENS:PN1:CORR:COUN ' + str(n_corr))

    def get_correlation(self):
        """
            Get number of correlations setting (Avg/BW -> Correlation)
			
            return:
                n_corr: number correlation points
        """
        n_corr = float(self.query(':SENS:PN1:CORR:COUN?'))
        return n_corr

    def get_number_points(self):
        """
            Get number of measurement points
			
            return:
                n_points: number of measurement points
        """
        n_points = float(self.query(':SENS:PN1:SWE:POIN?'))
        return n_points

    def get_lo_opt(self):
        """
            Get Local Oscillator Optimization Setting

            return:
              lob_str: string which can be 'LO Opt > 150kHz' or 'LO Opt < 150kHz'
        """
        lob_str = self.query(':SENS:PN1:LOB?')
        if('NARR' in lob_str)   : lob_str = 'LO Opt > 150kHz'
        elif('WIDE' in lob_str) : lob_str = 'LO Opt < 150kHz'
        return lob_str

    def get_measurement_quality(self):
        """
            Get Measurement Quality

            return:
              mes_qual_str: string which can be 'Normal' or 'Fast'
        """
        meas_qual_str = self.query(':SENS:PN1:SEGT:MEAS:QUAL?')
        if('NORM' in meas_qual_str)   : meas_qual_str = 'Normal'
        elif('FAST' in meas_qual_str) : meas_qual_str = 'FAST'
        return meas_qual_str

    def get_carrier(self):
        """
            Get carrier characteristics (frequency and power)
			
            return:
                [f_carrier, p_carrier]: f_carrier is the carrier frequency in Hz and p_carrier is the carrier power in dBm
        """
        carrier = (self.query(':CALC:PN1:DATA:CARR?')).split(',')
        return [float(i) for i in carrier]

    def get_xdat(self):
        """
            Get frequency data from measurement

            return:
                [f_0, f_1, f_2, ...]: f_i is the frequency for point i in Hz
        """
        xdata = (self.query(':CALC:PN1:DATA:XDAT?')).split(',')
        return [float(i) for i in xdata]

    def get_fdat(self):
        """
            Get formatted phase-noise data from measurement

            return:
                [pn_0, pn_1, pn_2, ...]: pn_i is the phase-noise for point i in dBc/Hz
        """
        fdata = (self.query(':CALC:PN1:TRAC1:DATA:FDAT?')).split(',')
        return [float(i) for i in fdata]

    def get_pn_analysis_integral(self):
        """
            Get integrated phase-noise analysis results from the instrument

            return:
                [Integ_noise, Freq_range, Rms_rad, Rms_deg, jitter, Residual_fm]
        """
        int_pn = (self.query(':CALC:PN1:TRAC1:FUNC:INT:DATA?')).split(',')
        return [float(i) for i in int_pn]

    def config_average_trigger(self):
        """
            Configure for average trigger
        """
        self.send(':TRIG:MODE PN1')
        self.send(':TRIG:PN1:SOUR INT')
        self.send(':INIT:PN1:CONT 0')
        self.send(':TRIG:AVER 1')

    def average_trigger(self):
        """
            Run average trigger
        """
        self.send(':INIT:PN1:IMM')

    def config_bus_trigger(self):
        """
            Configure for bus trigger
        """
        self.send(':TRIG:MODE PN1')
        self.send(':TRIG:PN1:SOUR BUS')
        self.send(':INIT:PN1:CONT 1')

    def bus_trigger(self):
        """
            Run bus trigger
        """
        self.send('*TRG')

    def check_measurement_running(self):
        """
            Check if a measurement is running

            return:
              0/1: measurement is finished/running
        """
        return (int(self.query(':STAT:OPER:COND?')) >> 4) & 0x0001

    def adjust_frequency_band(self):
        """
            Adjusts the instrument frequency band for the measured carrier frequency

            return:
              0/1: frequency do not correspond to any frequency band/frequency band was adjusted
        """

        # Remember detected carrier in every frequency band
        carr = {}
        for i in [1,2,3,4]:
            self.set_fband(i)
            carr[i] = self.get_carrier()

        # Find carrier with the highest power
        maxP = -1000
        maxP_band = 0
        for (i,(f,p)) in carr.items():
            self.logger.info('Frequency band: %d Carrier frequency: %5.2f MHz Power: %2.2f dBm' % (i, f/1e6, p))
            if(p > maxP):
                maxP = p
                maxP_band = i

        # Set the SSA to frequency band in which we found carrier with the highest power
        self.logger.info('Selected Frequency band for carrier detection: %d' % maxP_band)
        self.set_fband(maxP_band)

        # Analyze carrier frequency
        carrier_frequency = self.get_carrier()[0]

        if(carrier_frequency > 250.5e6 and carrier_frequency <= 7e9):
            self.set_fband(4)
            self.logger.info('adjust_frequency_band: Carrier frequency (%5.2f MHz), adjusted at BAND4 (250MHz - 7GHz)' % (carrier_frequency/1e6))
            return 1
        elif(carrier_frequency > 99.5e6 and carrier_frequency <= 250.5e6):
            self.set_fband(3)
            self.logger.info('adjust_frequency_band: Carrier frequency (%5.2f MHz), adjusted at BAND3 (99MHz - 1.5GHz)' % (carrier_frequency/1e6))
            return 1
        elif(carrier_frequency > 39.5e6 and carrier_frequency <= 99.5e6):
            self.set_fband(2)
            self.logger.info('adjust_frequency_band: Carrier frequency (%5.2f MHz), adjusted at BAND2 (39MHz - 101MHz)' % (carrier_frequency/1e6))
            return 1
        elif(carrier_frequency > 9.9e6 and carrier_frequency <= 39.5e6):
            self.set_fband(1)
            self.logger.info('adjust_frequency_band: Carrier frequency (%5.2f MHz), adjusted at BAND1 (10MHz - 41MHz)' % (carrier_frequency/1e6))
            return 1
        else:
            self.logger.info('adjust_frequency_band: Carrier frequency (%5.2f MHz), no band available (10MHz - 7GHz)' % (carrier_frequency/1e6))
            return 0
	
    def adjust_power_level(self):
        """
            Adjusts the attenuator so the power level is in the range 0dBm to 5dBm (recommended in instrument help section "Setting Attenuator")
            Checks also error handler

            return:
              0/1: error in questionable power register/attenuator was adjusted properly
        """

        # Analyze carrier power
        carrier_power = self.get_carrier()[1]

        # Check power-related errors
        status_power = self.questionable_error_handling(0,0,1,0)

        if(status_power[0] == 0):
            self.logger.error('adjust_power_level: Following errors were identified: ' + status_power[1] + ' DUT Power = %5.2f dBm' % carrier_power)
            return 0

        attenuation = (math.ceil(carrier_power/5) - 1)*5        	
        if(attenuation < 0) : attenuation = 0

        self.set_attenuation(attenuation)
        self.logger.info('adjust_power_level: Attenuation was set to %5.2f dB, for DUT power of %5.2f dBm' % (attenuation, carrier_power))

        return 1

    def run_measurement_routine(self):
        """
            Main instrument routine run with average trigger
            OBS1: This routine sets a sticky bit on the PHASE SUMMARY for the IF A/D overflow bit (common occurrence on measurements)
                  In case of IF_AD_OVERFLOW error or PHASE_SUMMARY, the routine tries to re-adjust IF gain to a lower value and re-run measurement
                  More details are available on Appendix B of programming manual and instrument Help Button

            return:
              [1, '']: measurement finished with no errors
              [0, 'ERROR_NAME0; ERROR_NAME1; ...;']: error happened
        """
        run_measurement = 1
        while(run_measurement):
            self.clear_averaging()
            self.config_average_trigger()
            
            # Enable phase summary on event of IF A/D overflow bit
            self.send(':STAT:QUES:PHAS:ENAB 8')
            #self.send(':STAT:QUES:PTR 64')
            
            # Clear status registers
            self.cls()

            # Enable trigger and wait for measurement to finish
            self.average_trigger()
            wait_measurement = True
            while(wait_measurement):
                measurement_running      = self.check_measurement_running()
                measurement_error_status = self.questionable_error_handling(1,1,1,1)
                wait_measurement = measurement_running and measurement_error_status[0]
                time.sleep(0.1)

            # Check error status of measurement
            if(measurement_error_status[0]):
                run_measurement = 0
                self.logger.info('run_measurement_routine: Measurement finished on SSA with no errors')
            # Try to adjust IF GAIN in case this was the error
            elif( ('IF_AD_OVERFLOW'      in measurement_error_status[1]) or
                  ('PHASE_SUMMARY'       in measurement_error_status[1]) ):
                if_gain = self.get_if_gain()
                if(if_gain > 0):
                    run_measurement = 1
                    self.logger.warn('run_measurement_routine: Measurement error - ' + measurement_error_status[1])
                    self.logger.info('run_measurement_routine: Trying to adjust IF gain and re-start measurement')
                    self.set_if_gain(if_gain - 10)
                else:
                    run_measurement = 0
                    self.abort_measurement()
                    self.logger.error('run_measurement_routine: IF A/D OVERFLOW IDENTIFIED, but IF gain is set to minimum value 0dB')
            # Error occurred and do not know automatically how to adjust
            else: 
                run_measurement = 0
                self.abort_measurement()
                self.logger.error('run_measurement_routine: Measurement error - ' + measurement_error_status[1])

        return measurement_error_status

    def abort_measurement(self):
        """
            Aborts SSA measurement
        """
        self.send(':ABOR')

    def questionable_error_handling(self, read_global_quest=0, read_status_quest=0, read_event_power=0, read_event_phase=0):
        """
            Identifies Questionable Error in the instrument - more details are available on Appendix B of programming manual
            OBS: not all the errors conditions were implemented yet in this function

            args: 
              read_global_quest: (0/1) Read Global Questionable Status Event Register? (obs: prior enabling needed)
              read_status_quest: (0/1) Read Questionable Status Event Register?        (obs: prior enabling needed)
              read_event_power : (0/1) Read Questionable Power Status Event Register?
              read_event_phase : (0/1) Read Questionable Phase Status Event Register?
			  
            return:
              [1, '']: no errors identified
              [0, 'ERROR_NAME0; ERROR_NAME1; ...;']: error happened
        """

        error_vector = [1]
        error_vector.append('')

        # Read Global Questionable Status Event Register
        if(read_global_quest):
            global_questionable       = int(self.query(':STAT:QUES?'))
            STAT_QUES_INTERRUPT       = (global_questionable >> 0)  & 0x0001

            if(STAT_QUES_INTERRUPT):
                error_vector[0] = 0
                error_vector[1] = error_vector[1] + ('STAT_QUES_INTERRUPT;')

        if(read_status_quest):
            # Read All Questionable Status Event Register
            status_questionable       = int(self.query(':STAT:QUES:COND?'))
            CURRENT_SUMMARY           = (status_questionable >> 1)  & 0x0001
            POWER_SUMMARY             = (status_questionable >> 3)  & 0x0001
            RF_FREQ_OUT_OF_RANGE      = (status_questionable >> 5)  & 0x0001
            PHASE_SUMMARY             = (status_questionable >> 6)  & 0x0001
            LIMIT_SUMMARY             = (status_questionable >> 9)  & 0x0001
            REFERENCE_SIGNAL_SUMMARY  = (status_questionable >> 10) & 0x0001
            MISC_SUMMARY              = (status_questionable >> 11) & 0x0001
            POWER_ON_TEST_FAIL        = (status_questionable >> 12) & 0x0001
            DCONVERTER_SUMMARY        = (status_questionable >> 13) & 0x0001

            if(CURRENT_SUMMARY):
                error_vector[0] = 0
                error_vector[1] = error_vector[1] + ('CURRENT_SUMMARY;')
            if(POWER_SUMMARY):
                error_vector[0] = 0
                error_vector[1] = error_vector[1] + ('POWER_SUMMARY;')
            if(RF_FREQ_OUT_OF_RANGE):
                error_vector[0] = 0
                error_vector[1] = error_vector[1] + ('RF_FREQ_OUT_OF_RANGE;')
            if(PHASE_SUMMARY):
                error_vector[0] = 0
                error_vector[1] = error_vector[1] + ('PHASE_SUMMARY;')
            if(LIMIT_SUMMARY):
                error_vector[0] = 0
                error_vector[1] = error_vector[1] + ('LIMIT_SUMMARY;')
            if(REFERENCE_SIGNAL_SUMMARY):
                error_vector[0] = 0
                error_vector[1] = error_vector[1] + ('REFERENCE_SIGNAL_SUMMARY;')
            if(MISC_SUMMARY):
                error_vector[0] = 0
                error_vector[1] = error_vector[1] + ('MISC_SUMMARY;')
            if(POWER_ON_TEST_FAIL):
                error_vector[0] = 0
                error_vector[1] = error_vector[1] + ('POWER_ON_TEST_FAIL;')
            if(DCONVERTER_SUMMARY):
                error_vector[0] = 0
                error_vector[1] = error_vector[1] + ('DCONVERTER_SUMMARY;')

        # Read Questionable Power Status Event Register?
        if(read_event_power):
            status_power          = int(self.query(':STAT:QUES:POW:EVEN?'))
            RF_LEVEL_INSUFFICIENT = (status_power >> 0) & 0x0001
            RF_LEVEL_OVERLOAD     = (status_power >> 1) & 0x0001
            IF_LEVEL_INSUFFICIENT = (status_power >> 2) & 0x0001
            IF_LEVEL_OVERLOAD     = (status_power >> 3) & 0x0001
            
            if(RF_LEVEL_INSUFFICIENT):
                error_vector[0] = 0
                error_vector[1] = error_vector[1] + ('RF_LEVEL_INSUFFICIENT;')
            if(RF_LEVEL_OVERLOAD):
                error_vector[0] = 0
                error_vector[1] = error_vector[1] + ('RF_LEVEL_OVERLOAD;')
            if(IF_LEVEL_INSUFFICIENT):
                error_vector[0] = 0
                error_vector[1] = error_vector[1] + ('IF_LEVEL_INSUFFICIENT;')
            if(IF_LEVEL_OVERLOAD):
                error_vector[0] = 0
                error_vector[1] = error_vector[1] + ('IF_LEVEL_OVERLOAD;')

        # Read Questionable Phase Status Event Register?
        if(read_event_phase):
            status_phase             = int(self.query(':STAT:QUES:PHASE:EVEN?'))
            PHASE_LOCK_LOOP_UNLOCKED = (status_power >> 0) & 0x0001
            PLL_FREQ_OUT_OF_RANGE    = (status_power >> 1) & 0x0001
            PLL_INPUT_OVERFLOW       = (status_power >> 2) & 0x0001
            IF_AD_OVERFLOW           = (status_power >> 3) & 0x0001
            
            if(PHASE_LOCK_LOOP_UNLOCKED):
                error_vector[0] = 0
                error_vector[1] = error_vector[1] + 'PHASE_LOCK_LOOP_UNLOCKED;'
            if(PLL_FREQ_OUT_OF_RANGE):
                error_vector[0] = 0
                error_vector[1] = error_vector[1] + 'PLL_FREQ_OUT_OF_RANGE;'
            if(PLL_INPUT_OVERFLOW):
                error_vector[0] = 0
                error_vector[1] = error_vector[1] + 'PLL_INPUT_OVERFLOW;'
            if(IF_AD_OVERFLOW):
                error_vector[0] = 0
                error_vector[1] = error_vector[1] + 'IF_AD_OVERFLOW;'

        return error_vector

    def save_screen_ssa(self, filename='F:/dummy.png'):
        """
            Save screen image to the instrument local hard-disk

            args:
              filename = string for filename with .png format
        """
        self.send(':MMEM:STOR:IMAG "' + filename + '"')

    def get_screen_ssa(self):
        """
            Get screen image in png format

            return:
              screen image in png format
        """
        dummy_filename = 'F:/dummy.png'
        self.save_screen_ssa(dummy_filename)
        screen = self.get_file_ssa(dummy_filename)
        self.delete_file_ssa(dummy_filename)
        return screen

    def get_file_ssa(self, filename):
        """
            Get file from the instruments local hard-disk

            args:
              filename = string as a filename
            return:
              requested file
        """
        return self.query_binary_values(':MMEM:DATA? "' + filename + '"', datatype='B',
                                        is_big_endian=True, container=bytearray)

    def delete_file_ssa(self, filename):
        """
            Delete file from the instruments local hard-disk

            args:
             filename = string as a filename
        """
        self.send(':MMEM:DEL "' + filename + '"')

    def save_phase_noise_data(self, save_to_file=1, filename='./dummy'):
        """
            Get frequency and formatted phase-noise data and save to .txt file			
            Saves also configuration of the instrument

            args:
              save_to_file (0/1)
              filename = string for filename

            return:
              [freq, phase_noise]
              freq = [f_0, f_1, f_2, ...]: f_i is the frequency for point i in Hz
              phase_noise = [pn_0, pn_1, pn_2, ...]: pn_i is the phase-noise for point i in dBc/Hz
        """

        freq        = self.get_xdat()
        phase_noise = self.get_fdat()

        if(save_to_file):
            # Get configuration
            [carrier_freq, carrier_power] = self.get_carrier()
            fband                         = self.get_fband()
            attenuation                   = self.get_attenuation()
            [start_offset, stop_offset]   = self.get_offset()
            [n_averaging,en_averaging]    = self.get_averaging()
            xcorrelation                  = self.get_correlation()
            if_gain                       = self.get_if_gain()
            lo_opt                        = self.get_lo_opt()
            meas_quality                  = self.get_measurement_quality()
            number_points                 = self.get_number_points()

            # Get integral phase noise analysis
            self.set_pn_analysis_range_full()
            [integ_noise, freq_range, rms_rad, rms_deg, jitter, residual_fm] = self.get_pn_analysis_integral()

			
            if(fband==1)   : fband_str = '1 (10M-41MHz)'
            elif(fband==2) : fband_str = '2 (39M-101MHz)'
            elif(fband==3) : fband_str = '3 (99M-1.5GHz)'	
            elif(fband==4) : fband_str = '4 (250M-7GHz)'
            else           : fband_str = str(fband) + '(Unrecognized band)'				

            # Save
            with open(filename + '.txt', 'w') as f:
                # Write configuration
                f.write('# %30s,%30s\n'   % ('Date-Time'   , str(datetime.datetime.now()) ))				
                f.write('# %30s,%30.5f\n' % ('Carrier Frequency (MHz)', (carrier_freq/1e6)))
                f.write('# %30s,%30.5f\n' % ('Carrier Power (dBm)'    , carrier_power     ))
                f.write('# %30s,%30s\n'   % ('Frequency Band'         , fband_str         ))
                f.write('# %30s,%30d\n'   % ('Attenuation (dB)'       , attenuation       ))
                f.write('# %30s,%30.5f\n' % ('Start Offset (Hz)'      , start_offset      ))
                f.write('# %30s,%30.5f\n' % ('Stop Offset (MHz)'      , (stop_offset/1e6) ))
                f.write('# %30s,%30d\n'   % ('Number Averaging'       , n_averaging       ))
                f.write('# %30s,%30d\n'   % ('Enable Averaging'       , en_averaging      ))
                f.write('# %30s,%30d\n'   % ('Cross-Correlation'      , xcorrelation      ))
                f.write('# %30s,%30d\n'   % ('IF Gain (dB)'           , if_gain           ))
                f.write('# %30s,%30s\n'   % ('LO Optimization '       , lo_opt            ))
                f.write('# %30s,%30s\n'   % ('Measurement Quality'    , meas_quality      ))
                f.write('# %30s,%30d\n'   % ('Number Points'          , number_points     ))
                f.write('# Integral phase noise analysis:\n')
                f.write('# %30s,%30.5f\n' % ('Integral noise (dBc)'   , integ_noise       ))
                f.write('# %30s,%30.5f\n' % ('Frequency range (Hz)'   , freq_range        ))
                f.write('# %30s,%30.9f\n' % ('RMS noise in radians'   , rms_rad           ))
                f.write('# %30s,%30.7f\n' % ('RMS noise in degrees'   , rms_deg           ))
                f.write('# %30s,%30.6f\n' % ('RMS jitter (ps)'        , jitter*1e12       ))
                f.write('# %30s,%30.5f\n' % ('Residual FM (Hz)'       , residual_fm       ))
                f.write('#\n')

                # Write frequency x phase-noise data
                f.write('  %30s,%30s\n' % ('Frequency (Hz)', 'Phase-Noise (dBc/Hz)'))
                for i in range(0,len(freq)):
                    f.write('  %30.5f,%30.5f\n' % (freq[i], phase_noise[i]))				

        return [freq, phase_noise]