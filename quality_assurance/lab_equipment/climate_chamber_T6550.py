#!/usr/bin/env python

from . driver_comm import DriverComm
import time

# -------------------------------------------------------------
#  ------------------------- Class CTS ------------------------
# -------------------------------------------------------------

class CTS(DriverComm):

    """
        CTS is a low-level class to send/receive information (Ethernet) to the Climate Chamber present in the timing lab
        The documentation of this equipment can be found in the manual present in the timing lab
        OBS: It is NOT recommended to use this class directly but to use the class ClimateChamber (which inherits from CTS)
    """

    def __init__(self, ip='192.168.1.90', port=1080, loggername=None):
        """
            Class constructor

            args:			
              ip          : IP address of chamber in format 'XXX.XXX.XXX.XXX'
              port        : Port Number of the class (integer)
              loggername  : string of logger name handling logging			 
        """
        DriverComm.__init__(self, ip, port, 0, 1, loggername)

    def set_time(self, DD, MM, YY, hh, mm, ss):
        msg = 't' + DD + MM + YY + hh + mm + ss
        msg = self.query(msg).decode("utf-8")
        return msg[1:3], msg[3:5], msg[5:7], msg[7:9], msg[9:11], msg[11:]

    def read_time(self):
        msg = self.query('T').decode("utf-8")
        return msg[1:3], msg[3:5], msg[5:7], msg[7:9], msg[9:11], msg[11:]

    def set_digital_channel_chamber(self, channel, state):
        msg = 's' + str(channel) + ' ' + str(state)
        return self.query(msg).decode("utf-8")

    def read_chamber_state(self):
        return self.query('S').decode("utf-8")

    def set_program_state(self, program):
        msg = 'p' + program
        return self.query(msg).decode("utf-8")

    def read_program_info(self, command):
        msg = 'p' + command
        return self.query(msg).decode("utf-8")

    def read_program_state(self):
        return self.query('P').decode("utf-8")

    def read_all_error_text(self, command):
        msg = 'H' + str(command)
        return self.query(msg).decode("utf-8")

    def read_error_text(self):
        return self.query('F').decode("utf-8")

    def set_keyboard_lock(self, state):
        msg = 'l' + str(state)
        return self.query(msg).decode("utf-8")

    def read_keyboard_lock(self):
        return self.query('L').decode("utf-8")

    def set_additional_digital_channel(self, channel, value):
        msg = 'o0' + str(channel) + ' ' + value
        return self.query(msg).decode("utf-8")

    def read_additional_digital_channel(self):
        return self.query('O').decode("utf-8")

    def set_analog_channel(self, channel, value):
        msg = 'a' + str(channel) + ' ' + value
        return self.query(msg).decode("utf-8")

    def read_analog_channel(self, channel):
        msg = 'A' + str(channel)
        return self.query(msg).decode("utf-8")
            
    def set_gradient_ramp_up(self, channel, value):
        msg = 'u' + str(channel) + ' ' + value
        return self.query(msg).decode("utf-8")
    
    def set_gradient_ramp_down(self, channel, value):
        msg = 'd' + str(channel) + ' ' + value
        return self.query(msg).decode("utf-8")
        
    def read_adjusted_gradient(self, channel):
        msg = 'U' + str(channel)
        return self.query(msg).decode("utf-8")
    
    def read_adjusted_final_ramp(self, channel):
        msg = 'E' + str(channel)
        return self.query(msg).decode("utf-8")
    
    def read_ramp_channel(self, channel):
        msg = 'R' + str(channel)
        return self.query(msg).decode("utf-8")

# -------------------------------------------------------------
#  ------------------------- Class ClimateChamber ------------------------
# -------------------------------------------------------------

class ClimateChamber(CTS):

    """
        ClimateChamber inherits from the CTS class and it implements higher-level methods to be used by a user
    """

    def __init__(self, ip='192.168.1.90', port=1080, loggername=None):
        """
            Class constructor

            args:			
              ip          : IP address of chamber in format 'XXX.XXX.XXX.XXX'
              port        : Port Number of the class (integer)
              loggername  : string of logger name handling logging			 
        """
        CTS.__init__(self, ip, port, loggername)

    def format_value(self, value):
        """
            formats value to use the CTS class

            args:			
              value : float
            return:
              value_str: value as a string with a format accepted by the CTS class
        """		
        if (isinstance(value, int)):
            value = float(value)
        if (isinstance(value, float)):
            result = str('%05.1f' % value)
            return result
        else:
            return "nan"

    def start(self):
        """
            Start chamber temperature control
        """	
        self.set_digital_channel_chamber(1, 1)
        self.logger.info('start: Climate Chamber has started')	

    def stop(self):
        """
            Stop chamber temperature control
        """		
        self.set_digital_channel_chamber(1, 0)
        self.logger.info('stop: Climate Chamber has stopped')	

    def set_temperature(self, value):
        """
            Set temperature chamber

            args:
              value: float for temperature in degC
        """	
        command = self.format_value(value)	
        self.set_analog_channel(0, command)
        self.logger.info('set_temperature: ClimateChamber set to ' + command + 'degC')	

    def get_temperature(self):
        """
            Get temperature chamber

            return:
              temperature: float for temperature in degC
        """	
        stat =  self.read_analog_channel(0)
        return float(stat[3:8])

    def wait_stable(self, temperature_target, tolerance=0.5, tolerance_time=30, sampling_time = 3):
        """
            Wait until chamber temperature reaches target temperature with a certain tolerance that must be true for a certain time
            This method does not set the chamber temperature, therefore the user shall set and start the chamber prior to the execution of this method
            While the chamber is not stable, this method prints the temperature logger in a INFORMATION severity at every ~30s and in a DEBUG severity at every ~1s
            args:
              temperature_target: target temperature in degC
              tolerance         : temperature_target - tolerance < actual_temperature < temperature_target + tolerance
                                  tolerance is given in degC
              tolerance_time    : tolerance_time for which the chamber must be within the limits defined above for it to be considered stable
                                  tolerance_time is given in seconds
              sampling_time     : time between two temperature reads given in seconds (recommended to leave at default)

            return:
              actual_temperature: actual chamber temperature in degC
        """	
        stable        = 0
        found_bounded = 0
        iteration_number = 0
        while(not stable): 
            actual_temperature = self.get_temperature()
            if(iteration_number%10 == 0)   : self.logger.info('wait_stable: Current temperature: ' + str(actual_temperature) + 'degC')
            else                           : self.logger.debug('wait_stable: Current temperature: ' + str(actual_temperature) + 'degC')
            if((actual_temperature > (temperature_target-tolerance)) and
               (actual_temperature < (temperature_target+tolerance)) ):
                if(not found_bounded):
                    found_bounded = 1
                    time_0 = time.time()
                else:
                    time_now = time.time()
                    if((time_now - time_0) >= tolerance_time) : stable = 1
            else:
                found_bounded = 0
            time.sleep(sampling_time)
            iteration_number = iteration_number + 1
        self.logger.info('wait_stable: Temperature Stable Reached')

        return actual_temperature
