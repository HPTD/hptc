#!/usr/bin/env python

import logging
import math
import csv
import os
import datetime

# -------------------------------------------------------------------------------------------------------------
#  -------------------- Class SFP_SFF8472 (Digital Diagnostic monitoring for SFP) ------------------
# -------------------------------------------------------------------------------------------------------------

# Class implemented by EBSM in order to read digital diagnostic monitoring for SFP's as specified by the SFF-8472 standard
class SFP_SFF8472:

    def __init__(self, scale_temp=1, scale_vcc=1, scale_tx_bias=1, scale_tx_pwr=1, scale_rx_pwr=1):
        self.scale_temp    = scale_temp
        self.scale_vcc     = scale_vcc
        self.scale_tx_bias = scale_tx_bias
        self.scale_tx_pwr  = scale_tx_pwr
        self.scale_rx_pwr  = scale_rx_pwr
        self.logger = logging.getLogger('ttc_pon')

    # The daughter class shall implement the following method which will be overwritten
    # returns a list where the first element indicates success (0,1) and the others are the read data values
    # The SFP methods below assume that data_list is at least a single byte and nbr_read can be at least 2 (apart from save_regbank which assumes 256 bursty-read)
    def i2c_writeread(self,slv_addr, nbr_read, data_list):
        pass

    def get_sfp_vendor(self):  
        slv_addr = 0x50
        reg_bank = []
        for reg_addr in range(20,36,2):
            reg_value = self.i2c_writeread(slv_addr,2, [reg_addr]) # double burst-read access
            success = reg_value[0]
            if(not success):
                self.logger.warn('Failed to read from SFP VENDOR')
                return [0, '%16s '% 'N/A']
            reg_value1 = reg_value[1]
            reg_value2 = reg_value[2]			
            reg_bank.append(reg_value1)
            reg_bank.append(reg_value2)
        vendor_name = ''.join(chr(i) for i in reg_bank)
        return [1, vendor_name]

    def get_sfp_part_number(self):  
        slv_addr = 0x50
        reg_bank = []
        for reg_addr in range(40,56,2):		
            reg_value = self.i2c_writeread(slv_addr,2, [reg_addr]) # double burst-read access
            success = reg_value[0]
            if(not success):
                self.logger.warn('Failed to read from SFP PART NUMBER')
                return [0, '%16s '% 'N/A']
            reg_value1 = reg_value[1]
            reg_value2 = reg_value[2]		
            reg_bank.append(reg_value1)
            reg_bank.append(reg_value2)			
        vendor_part_number = ''.join(chr(i) for i in reg_bank)
        return [1, vendor_part_number]

    def get_sfp_serial_number(self):  
        slv_addr = 0x50
        reg_bank = []
        for reg_addr in range(68,84,2):
            reg_value = self.i2c_writeread(slv_addr,2, [reg_addr]) # double burst-read access
            success = reg_value[0]
            if(not success):
                self.logger.warn('Failed to read from SFP SERIAL NUMBER')
                return [0, '%16s '% 'N/A']
            reg_value1 = reg_value[1]
            reg_value2 = reg_value[2]	
            reg_bank.append(reg_value1)
            reg_bank.append(reg_value2)
        vendor_serial_number = ''.join(chr(i) for i in reg_bank)
        return [1, vendor_serial_number]

    def get_sfp_temp(self):  
        slv_addr  = 0x51
        # Read digital diagnostic
        reg_bank = []
        reg_addr = 96
        reg_value  = self.i2c_writeread(slv_addr,2, [reg_addr]) # double burst-read access
        success = reg_value[0]
        if(not success):
            self.logger.warn('Failed to read from SFP TEMP')
            return [0, float('nan')]
        reg_value1 = reg_value[1]
        reg_value2 = reg_value[2]			
        reg_bank.append(reg_value1)
        reg_bank.append(reg_value2)

        temp    = (reg_bank[0]<<8) + reg_bank[1]

        # TEMPERATURE:
        # 1) Internally measured transceiver temperature. Represented as a 16 bit signed twos complement value in
        # increments of 1/256 degrees Celsius
        if(temp&0x8000):
            sign = -1
            temp_conv = (temp ^ 0xFFFF) + 1
        else:
            sign = 1
            temp_conv = temp
        temp_conv = sign*temp_conv*self.scale_temp*(1.0/256.0) # UNIT: degrees Celsius

        return [1, temp_conv]

    def get_sfp_vcc(self):  
        slv_addr  = 0x51
        # Read digital diagnostic
        reg_bank = []
        reg_addr = 98
        reg_value  = self.i2c_writeread(slv_addr,2, [reg_addr]) # double burst-read access
        success = reg_value[0]
        if(not success):
            self.logger.warn('Failed to read from SFP VCC')
            return [0, float('nan')]
        reg_value1 = reg_value[1]
        reg_value2 = reg_value[2]			
        reg_bank.append(reg_value1)
        reg_bank.append(reg_value2)

        vcc     = (reg_bank[0]<<8) + reg_bank[1]

        # SUPPLY VOLTAGE:
        # 2) Internally measured transceiver supply voltage. Represented as a 16 bit unsigned integer with the
        # voltage defined as the full 16 bit value (0-65535) with LSB equal to 100 uVolt
        vcc_conv = (vcc*self.scale_vcc*100e-6) # UNIT: V

        return [1, vcc_conv]

    def get_sfp_tx_bias(self):  
        slv_addr  = 0x51
        # Read digital diagnostic
        reg_bank = []
        reg_addr = 100
        reg_value  = self.i2c_writeread(slv_addr,2, [reg_addr]) # double burst-read access
        success = reg_value[0]
        if(not success):
            self.logger.warn('Failed to read from SFP TX BIAS')
            return [0, float('nan')]
        reg_value1 = reg_value[1]
        reg_value2 = reg_value[2]			
        reg_bank.append(reg_value1)
        reg_bank.append(reg_value2)

        tx_bias = (reg_bank[0]<<8) + reg_bank[1]	

		# TX BIAS CURRENT:
        # 3) Measured TX bias current in uA. Represented as a 16 bit unsigned integer with the current defined as the
        # full 16 bit value (0-65535) with LSB equal to 2 uA		
        tx_bias_conv = (tx_bias*2e-6 * self.scale_tx_bias)*1e3 # UNIT: mA

        return [1, tx_bias_conv]
		
    def get_sfp_tx_pwr(self):  
        slv_addr  = 0x51
        # Read digital diagnostic
        reg_bank = []
        reg_addr = 102
        reg_value  = self.i2c_writeread(slv_addr,2, [reg_addr]) # double burst-read access
        success = reg_value[0]
        if(not success):
            self.logger.warn('Failed to read from SFP TX PWR')
            return [0, float('nan')]
        reg_value1 = reg_value[1]
        reg_value2 = reg_value[2]			
        reg_bank.append(reg_value1)
        reg_bank.append(reg_value2)

        tx_pwr  = (reg_bank[0]<<8) + reg_bank[1]

		# TX OUTPUT POWER:
        # 4) Measured TX output power in mW. Represented as a 16 bit unsigned integer with the power defined as
        # the full 16 bit value (0-65535) with LSB equal to 0.1 uW
        if(tx_pwr>0):
            tx_pwr_conv = 10*math.log10((tx_pwr * 0.1 * self.scale_tx_pwr * 1e-6)/1e-3) # UNIT: dBm
        else:
            tx_pwr_conv = -1*float('inf')		


        return [1, tx_pwr_conv]

    def get_sfp_rx_pwr(self):  
        slv_addr  = 0x51
        # Read digital diagnostic
        reg_bank = []
        reg_addr = 104
        reg_value  = self.i2c_writeread(slv_addr,2, [reg_addr]) # double burst-read access
        success = reg_value[0]
        if(not success):
            self.logger.warn('Failed to read from SFP RX PWR')
            return [0, float('nan')]
        reg_value1 = reg_value[1]
        reg_value2 = reg_value[2]		
        reg_bank.append(reg_value1)
        reg_bank.append(reg_value2)

        rx_pwr  = (reg_bank[0]<<8) + reg_bank[1]
        #print('rx_pwr:' + str(rx_pwr)) - DEBUGGING
		# RX RECEIVED OPTICAL POWER:
        # 5) Measured RX received optical power in mW. Value can represent either average received power or OMA
        # depending upon how bit 3 of byte 92 (A0h) is set. Represented as a 16 bit unsigned integer with the
        # power defined as the full 16 bit value (0-65535) with LSB equal to 0.1 uW
        if(rx_pwr>0):
            rx_pwr_conv = 10*math.log10((rx_pwr * 0.1 * self.scale_rx_pwr * 1e-6)/1e-3) # UNIT: dBm
        else:
            rx_pwr_conv = -1*float('inf')
			
        return [1, rx_pwr_conv]

    # returns list with IIC parameters
    def save_sfp_parameters(self, name_file, iteration=0): 
        # Create Formatted Vector
        fieldnames = ['%20s' % 'iteration',
                      '%20s' % 'date',
                      '%20s' % 'time',
                      '%20s' % 'temperature_degC',
                      '%20s' % 'vcc_V',					  
                      '%20s' % 'tx_bias_mA',
                      '%20s' % 'tx_pwr_dBm',
                      '%20s' % 'rx_pwr_dBm']

        # Get SFP parameters
        parameters_sfp = []
        parameters_sfp.append(self.get_sfp_temp()[1])
        parameters_sfp.append(self.get_sfp_vcc()[1])
        parameters_sfp.append(self.get_sfp_tx_bias()[1])
        parameters_sfp.append(self.get_sfp_tx_pwr()[1])
        parameters_sfp.append(self.get_sfp_rx_pwr()[1])

        date_time = datetime.datetime.now()
        measurements = { '%20s' % 'iteration'        : '%20d' % iteration,
                         '%20s' % 'date'             : '%20s' % date_time.date(),
                         '%20s' % 'time'             : '%20s' % date_time.time(),						 
                         '%20s' % 'temperature_degC' : '%20.10f' % parameters_sfp[0],
                         '%20s' % 'vcc_V'            : '%20.10f' % parameters_sfp[1],			
                         '%20s' % 'tx_bias_mA'       : '%20.10f' % parameters_sfp[2],
						 '%20s' % 'tx_pwr_dBm'       : '%20.10f' % parameters_sfp[3],
                         '%20s' % 'rx_pwr_dBm'       : '%20.10f' % parameters_sfp[4]}

        # Save Measurement
        # Check if file exists 
        file_exists = os.path.isfile(name_file + '.csv')  
        with open(name_file + '.csv', 'a') as csvfile:
            writer = csv.DictWriter(
                csvfile,
                fieldnames=fieldnames,
                delimiter=',',
                lineterminator='\n')
            if (not file_exists) : writer.writeheader()
            writer.writerow(measurements)

        return parameters_sfp

    # THIS METHOD ASSUMES THAT WRITEREAD CAN BE UP TO 256 BURST-READ
    # returns 1 for success and 0 if IIC failure
    def save_regbank(self, name_file, iteration=0): 
        # Create Formatted Vector
        fieldnames = ['%20s' % 'iteration',
                      '%20s' % 'date',
                      '%20s' % 'time',
                      '%20s' % 'slv_addr',
                      '%20s' % 'reg_addr',
                      '%20s' % 'reg_value']

        # Get SFP register bank
        regbank_A0 = self.i2c_writeread(0x50, 256, [0])
        regbank_A2 = self.i2c_writeread(0x51, 256, [0])

        date_time = datetime.datetime.now()


        # Save Measurement
        # Check if file exists 
        file_exists = os.path.isfile(name_file + '.csv')  
        with open(name_file + '.csv', 'a') as csvfile:
            writer = csv.DictWriter(
                csvfile,
                fieldnames=fieldnames,
                delimiter=',',
                lineterminator='\n')
            if (not file_exists) : writer.writeheader()
            for i in range(0,256):
                if(regbank_A0[0]) : reg_value = regbank_A0[i+1]
                else              : reg_value = float('nan')
                measurements = { '%20s' % 'iteration'        : '%20d' % iteration,
                                 '%20s' % 'date'             : '%20s' % date_time.date(),
                                 '%20s' % 'time'             : '%20s' % date_time.time(),						 
                                 '%20s' % 'slv_addr'         : '%20s' % '0x50',
                                 '%20s' % 'reg_addr'         : '%20d' % i,		
                                 '%20s' % 'reg_value'        : '%20.0f' % reg_value}
                writer.writerow(measurements)

            for i in range(0,256):
                if(regbank_A2[0]) : reg_value = regbank_A2[i+1]
                else              : reg_value = float('nan')
                measurements = { '%20s' % 'iteration'        : '%20d' % iteration,
                                 '%20s' % 'date'             : '%20s' % date_time.date(),
                                 '%20s' % 'time'             : '%20s' % date_time.time(),						 
                                 '%20s' % 'slv_addr'         : '%20s' % '0x51',
                                 '%20s' % 'reg_addr'         : '%20d' % i,		
                                 '%20s' % 'reg_value'        : '%20.0f' % reg_value}
                writer.writerow(measurements)

        return regbank_A0[0] & regbank_A2[0]