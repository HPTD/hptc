#!/usr/bin/env python

import math
import time
import datetime
from . telnet_comm import TelnetComm
import matplotlib.pyplot as plt
import numpy as np
# -------------------------------------------------------------
#  ------------- Class PhaseNoiseTest ----------------
# -------------------------------------------------------------

class PhaseNoiseTest(TelnetComm):

    """
        Class to control Phase Noise test set 5125A from Microsemi	
        Commands to this device can be found under CD available with instrument
    """

    def __init__(self, ip='192.168.1.2', port=1299, logger_name=None):
        """
            Class constructor

            args:			
              ip   : IP address as a string
              port : port number as a integer
              logger_name : logger_name application			  
        """		
        TelnetComm.__init__(self, ip, port, 10, logger_name)

    def take_control(self):
        """
            Take instrument control
        """	
        return self.query('control take')

    def yield_control(self):
        """
            Yield instrument control
        """	
        return self.query('control yield')

    #def reset(self):
    #    """
    #        Resets the test set (THIS IS A REBOOT)
    #    """
    #    return self.query('reset')
    #
    #def preset(self):
    #    """
    #        Presets the test set for factory default configurations  (THIS IS A REBOOT)
    #    """
    #    return self.query('restorefactorydefaults')

    def get_version(self):
        """
            Get instrument version
			
			return:
			    version: string with instrument version
        """
        return self.show('version')

    def set_phase_rate(self, rate):
        """
            Sets data port output rate in samples per second
			
			args:
			    rate: rate in samples per second, it can be 1, 10, 100 or 1000
        """
        return self.set('phaserate ' + str(rate))

    def get_phase_rate(self):
        """
            Gets data port output rate in samples per second
			
			return:
			    rate: rate in samples per second, it can be 1, 10, 100 or 1000
        """
        data = self.show('phaserate')
        return int(data.split('samples/sec')[0].split('phase output rate:')[1])

    def get_carrier_ref(self):
        """
            Gets frequency and power from INPUT and REF ports

			return:
			    [INPUT_frequency, INPUT_power, REF_frequency, REF_power]
                frequency unit is MHz
                power unit is dBm				
        """
        data = self.show('inputs')
        data = data.split('\r\n')
        in_aux = data[1]
        ref_aux = data[2]
        
        in_freq    = float(in_aux.split('Frequency')[1].split('MHz')[0])
        if(in_aux.find('Amplitude <')!= -1) : str_amplitude = 'Amplitude <'
        else : str_amplitude = 'Amplitude'		
        in_power   = float(in_aux.split(str_amplitude)[1].split('dBm')[0])

        if(ref_aux.find('IN=REF')!= -1):
            ref_freq = in_freq
            ref_power = in_power
        else:
            ref_freq    = float(ref_aux.split('Frequency')[1].split('MHz')[0])
            if(ref_aux.find('Amplitude <')!= -1) : str_amplitude = 'Amplitude <'
            else : str_amplitude = 'Amplitude'		
            ref_power   = float(ref_aux.split(str_amplitude)[1].split('dBm')[0])           		

        return [in_freq, in_power, ref_freq, ref_power]

    def set_tau0(self, tau0):
        """
            Sets tau0 (instrument phase sampling time)
			
			args:
			    tau0: tau0 value in seconds, it can be 0.001, 0.01, 0.1 or 1
        """
        return self.set('tau0 ' + str(tau0))

    def get_tau0(self):
        """
            Gets tau0 (instrument phase sampling time)
			
			return:
			    tau0: tau0 in seconds
        """
        data = self.show('tau0')
        return float(data.split('seconds')[0].split('tau0 is:')[1])

    def set_title(self, title):
        """
            Sets measurement title
			
			args:
			    title: quoted string up to 34 characters
        """
        return self.set('title ' + '"' + title + '"')

    def get_title(self):
        """
            Get measurement title
			
			return:
			    title: quoted string up to 34 characters
        """
        return self.show('title')

    def start(self):
        """
            Start instrument measurement
        """
        return self.query('start')

    def stop(self):
        """
            Stop instrument measurement
        """
        return self.query('stop')

    def get_status(self):
        """
            Get instrument status
			
			return:
			    [status, acquisition_time, error_message]
                status can be:
                   -1 - unknown state				
					0 - ready
					1 - collecting (acquisition time will show for how many minutes instrument has been collecting data)
					2 - initializing
                acquisition_time: acquisition time in minutes
                error_message: any error message present in instrument
        """
        data = self.show('state')
        data = data.split('\r\n\r')
        # Error message
        error_message = data[1]

        # State of instrument
        data = data[0]
        if(data.find('Ready') != -1)          : status = 0
        elif(data.find('Collecting') != -1)   : status = 1
        elif(data.find('Initializing') != -1) : status = 2
        else : status = -1

        # Acquisition time
        if(status==1):
            data = data.split('(')[1].split(')')[0]            
            if(data.find('h')!=-1):
                data = data.split('h')
                hours = int(data[0])
                data = data[1]
            else:
                hours = 0
            minutes = int(data.split('m')[0])
            acquisition_time = minutes + 60*hours
        else:
            acquisition_time = 0

        return [status, acquisition_time, error_message]
		
    def show(self, quantity = 'spectrum'):
        """
            Retrieve data from instrument
			
            args:
                quantity: quantity to be saved from instrument
            return:
                string raw data returned from instrument (with IP information removed)			
        """
        data = self.query('show ' + quantity)
        return data.rsplit('=' + self.IP)[0]

    def set(self, quantity = 'tau0 0.001'):
        """
            Set data

            args:
                quantity: data + value			
            return:
                string raw data returned from instrument (with IP information removed)			
        """
        data = self.query('set ' + quantity)
        return data.rsplit('=' + self.IP)[0]
		
    def save(self, quantity = 'spectrum', filename = 'dummy'):
        """
            Save data from instrument

            args:
               filename - string
            return:
                string returned from the instrument			
        """
        data = self.show(quantity)
        with open(filename + '.txt', 'w') as f:
            f.write(data)
        return data

    def save_adev(self, name_measurement='clock'):
        """
            Save Allan Deviation acquired data as txt file (name_measurement_adev.txt)
            args:
                name_measurement - string			
        """	
        return self.save('adev', name_measurement + '_adev')

    def save_phase_noise(self, name_measurement='clock'):
        """
            Save Phase Noise data, spur table and integrated phase-noise table
            files are saved as:
                name_measurement_spectrum.txt - phase noise spectrum
                name_measurement_ipn.txt      - integrated phase noise values
                name_measurement_spurs.txt	  - spur table
            args:
                name_measurement - string		
            return:
                [freq, data_pn]	
        """	
        self.save('ipn', name_measurement + '_ipn')
        self.save('spurs', name_measurement + '_spurs')
        data = self.save('spectrum', name_measurement + '_spectrum')		
        data = data.split('\r\n\r')
        data_pn = data[1].split('\r\n')
        freq    = []
        pn      = []
        for i in data_pn:
            aux = i.split('\t')
            freq.append(float(aux[0]))
            pn.append(float(aux[1]))

        return [freq, pn]

    def save_phasediff(self, name_measurement='clock'):
        """
            Return a snapshot of the phase difference acquired data as txt file (name_measurement_phasediff.txt)
            args:
                name_measurement - string
            return:
                phase difference data
        """	
        data = self.save('phasediff', name_measurement + '_phasediff')
        data = data.split('\r\n')
        print(data)
        phasediff    = []
        for i in range(1, len(data)-2):
            phasediff.append(float(data[i]))
        return phasediff

    def run_measurement(self, time_measurement):
        """
            Run measurement for a given quantity of minutes and check status

            args:
                time_measurement - measurement time in minutes
            return:
                success (0/1)
        """
        # Stop any potential on-going measurement
        self.stop()

        time.sleep(5)

        # Check instrument is ready
        status = self.get_status()
        if(status[0] != 0 or status[2]!=''):
            self.logger.warn('Instrument status is not ready or error message, please check instrument status')
            return 0

        # Start measurement		
        self.start()
        time.sleep(5)
        status = self.get_status()

        # Wait measurement to be finished
        measurement_elapsed = 0

        # Wait measurement
        while(measurement_elapsed < time_measurement and (status[0]==2 or status[0]==1)):
            time.sleep(30)
            status = self.get_status()
            measurement_elapsed = status[1]
            self.logger.info('Time elapsed for measurement running:' + str(measurement_elapsed) + 'm')

        # Check for errors
        if(status[0]!=1):
            self.logger.warn('Instrument acquisition not collecting, check error messages')
            self.logger.warn(status[2])
            return 0

        # Stop measurement
        self.stop()
        self.logger.info('Measurement run finished')

        return 1

class PhaseNoiseTestData(TelnetComm):

    """
        Class to control receive Phase measurement differences from test set 5125A from Microsemi	
        This is a read-only class
    """

    def __init__(self, ip='192.168.1.2', port=1298, logger_name=None):
        """
            Class constructor

            args:			
              ip   : IP address as a string
              port : port number as a integer
              logger_name : logger_name application
        """		
        TelnetComm.__init__(self, ip, port, 10, logger_name, False)

    def recv_data(self):
        """
            Receive data information

            return [success, [data0, data1, data2, data3, ...]]
        """	
        data = self.recv_available()
        if(data=='') :
            return [0]
        else:
            data = data.split('\r\n')
            data_float = [float(i) for i in data[0:-1]]
            return [1, data_float]

    def listen_save_plot(self, time_to_listen=60, time_idle_to_stop=10, navg = 1024, f_carrier=40e6, filename='data.txt', plot=True):
        """
            Receive data information, save to file and plot

            arg:
                time_to_listen in seconds is the maximum time the equipment will be capturing phase information
                time_idle_to_stop in seconds is the maximum time the equipment will wait for a new input before stopping
                filename to save data
                plot is a boolean to enable plot

            return:
                success: 0 when no phase was captured at all and time idle to stop has reached
                         1 when at least one phase was captured and the measurement finished either because of time_to_listen of time_idle_to_stop conditions
        """
        data = self.recv_available() # Flush existing data		
        k = 0
        y = []
        equipment_was_idle = 1
        always_idle = 1
        dtime = 0
        dtime_idle = 0
        t0 = time.time()
        t0_idle = t0
        while(dtime <= time_to_listen and dtime_idle<=time_idle_to_stop):
            data = self.recv_available()		
            if(data!='') :
                if(always_idle):
                    with open(filename, 'w') as f:
                        f.write('%20s,%20s,%30s\n' % ('Date','Time','Phase(ps)'))
                    always_idle = 0
                equipment_was_idle = 0
                k=k+1
                data = data.split('\r\n')
                data_float = []
                for i in data[0:-1]:			
                    if(float(i)<999) : data_float.append(float(i))
                aux = 0
                data_averaged = []
                #print(len(data_float))
                while(aux < len(data_float)):
                    data_averaged.append((1e12/f_carrier)*np.mean(data_float[aux:(aux+navg)]))
                    aux = aux + navg
                if(len(data_averaged) > 0):
                    with open(filename, 'a') as f:
                        date_time = datetime.datetime.now()
                        f.write('%20s,%20s,%30.20f\n' % (date_time.date(), date_time.time(),data_averaged[0]))
                        for i in range(1,len(data_averaged)):
                            f.write('%20s,%20s,%30.5f\n' % ('', '', data_averaged[i]))

                if(plot):						
                    if(k>60*10): y = y[len(data_averaged):] + data_averaged
                    else       : y = y + data_averaged
                    if(k==1) :
                        fig = plt.figure()
                        ax  = fig.add_subplot(1, 1, 1)
                        lines = ax.plot(y, color='b')
                    else:
                        l = lines.pop(0)
                        l.remove()
                        lines = ax.plot(y, color='b')
                    plt.xlabel('Sample')
                    plt.ylabel('Phase Difference (ps)')
                    plt.grid(True)
                    plt.title(filename)
                    plt.pause(0.1)
            else:
                if(equipment_was_idle):
                    dtime_idle = time.time() - t0_idle
                else:
                    t0_idle = time.time()
                    dtime_idle = 0					
                equipment_was_idle = 1
				
            dtime = time.time() - t0
        if(not always_idle):
            plt.close()
            return 1
        else:
            return 0