#!/usr/bin/env python

import telnetlib
import logging
import time
# -------------------------------------------------------------
#  -------------- Class TelnetComm (driver-layer) ------------
# -------------------------------------------------------------

class TelnetComm:

    def __init__(self, ip='192.168.1.2', port=9001, timeout=5, loggername=None, print_status = True):
        self.IP = ip
        self.PORT = port		
        self.tn = telnetlib.Telnet(self.IP, self.PORT, timeout) #timeout in s
        self.logger = logging.getLogger(loggername)		
        self.logger.info('Opening telnet communication at port:' + str(port) + ", ip:" + ip)
        time.sleep(1)		
        if(print_status) : print(self.recv_available())

    def send(self, data):
        data = data.encode('utf-8')
        if data[-1] != b'\n':
            data += b'\n'
        self.tn.write(data)
        return 1

    def recv_available(self):
        r = b'dummy'
        data = ''
        while(r!=b''):
            r=self.tn.read_eager()
            data = data + r.decode('utf-8')			
        return data

    def query(self, data):
        self.send(data)
        time.sleep(1)		
        return self.recv_available()

    def __del__(self):
        self.tn.close()