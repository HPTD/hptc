#!/usr/bin/env python

import math
import time
import datetime
from . visa_comm import VisaComm

# -------------------------------------------------------------
#  ----------------- Class PulseGenerator --------------------
# -------------------------------------------------------------

class PulseGenerator(VisaComm):

    """
        Class to control Pulse Generator (81134A) from KeySight	
        Commands to this device can be found under: 
        http://literature.cdn.keysight.com/litweb/pdf/5988-7401EN.pdf
    """

    def __init__(self, rm_visa, str_addr, logger_name):
        """
            Class constructor

            args:			
              rm_visa     : VISA resource manager (opened with rm_visa = visa.ResourceManager())
              str_addr    : VISA address, can be checked through Connection Expert application
                            for more information https://www.keysight.com/upload/cmc_upload/All/readme_IOLS_16_3_16603_3.htm?&cc=CN&lc=chi
              logger_name : string of logger name handling logging						 
        """		
        VisaComm.__init__(self, rm_visa, str_addr, logger_name)

    def get_id(self):
        """
            Get instrument ID

            return:
                instrument ID (check VISA reference for more information)
        """
        stat = self.query('*IDN?')
        return stat

    def cls(self):
        """
            Clear Status registers

            return:
                1 when instrument is operational
        """
        stat = self.send('*CLS')
        return int(self.query('*OPC?'))

    def preset(self):
        """
            Preset instrument and clear ctatus registers (Preset -> Factory)
        """
        stat = self.send(':SYST:PRES')
        self.cls()
        time.sleep(5)
    def set_mode_pattern(self, frequency):
        """
            Set continuous pattern for source output with a certain frequency

            args:
                frequency : unit is Hz		
        """
        self.send(':SOUR:FUNC:SHAP PATT')
        self.send(':SOUR:FREQ ' + str(frequency) + 'Hz')
        time.sleep(1)

    def set_mode_clock(self, channel=1):
        """
            Set clock pattern for a certain channel
			
            args:
                channel : 1 or 2
        """
        self.send(':SOUR:FUNC:MODE' + str(channel) + str(' SQU'))

    def enable_channel_pm(self, channel=1):
        """
            Enable external phase modulation for a channel
			
            args:
                channel : 1 or 2			
        """
        self.send(':SOUR:PM' + str(channel) + str(' ON'))

    def disable_channel_pm(self, channel=1):
        """
            Disable external phase modulation for a channel
			
            args:
                channel : 1 or 2			
        """
        self.send(':SOUR:PM' + str(channel) + str(' OFF'))

    def set_channel_pm_sensitivity(self, channel=1, sensitivity=0):
        """
            Set channel sensitivity to 50ps/V or 500ps/V
            OBS: between -500mV and 500mV the signal delay increases/decreases linearly to the Delay control input
            args:
                sensitivity : 0 = 25ps , 1 = 250ps	
        """
        if(sensitivity   == 0) : self.send(':SOUR:PM' + str(channel) + str(':SENS 25ps'))
        elif(sensitivity == 1) : self.send(':SOUR:PM' + str(channel) + str(':SENS 250ps'))

    def set_channel_voltage_level(self, channel=1, high=1.0, low=-1.0, term=0.0):
        """
            Set channel voltage level parameters
			
            args:
                channel : 1 or 2
                high    : HIGH voltage level in volts (-1.95V to +3.0V)	
                low     : LOW voltage level in volts (-2.0V to +2.95V)	
                term    : TERMINATION voltage level in volts (-2.0V to +3.0V)					
        """
        self.send(':SOUR:VOLT' + str(channel) + str(':LEV:IMM:HIGH ') + str(high) + 'V')
        self.send(':SOUR:VOLT' + str(channel) + str(':LEV:IMM:LOW ') + str(low)+ 'V')
        self.send(':SOUR:VOLT' + str(channel) + str(':LEV:IMM:TERM ') + str(term)+ 'V')

    def set_channel_voltage_level_amplitude(self, channel=1, amplitude=2.0, offset=0.0, term=0.0):
        """
            Set channel voltage level parameters
			
            args:
                channel : 1 or 2
                amplitude : AMPLITUDE voltage level in volts 
                offset    : OFFSET voltage level in volts 
                term      : TERMINATION voltage level in volts (-2.0V to +3.0V)					
        """
        self.send(':SOUR:VOLT' + str(channel) + str(':LEV:IMM:AMPL ') + str(amplitude) + 'V')
        self.send(':SOUR:VOLT' + str(channel) + str(':LEV:IMM:OFFS ') + str(offset)+ 'V')
        self.send(':SOUR:VOLT' + str(channel) + str(':LEV:IMM:TERM ') + str(term)+ 'V')

    def set_channel_voltage_level(self, channel=1, high=1.0, low=-1.0, term=0.0):
        """
            Set channel voltage level parameters
			
            args:
                channel : 1 or 2
                high    : HIGH voltage level in volts (-1.95V to +3.0V)	
                low     : LOW voltage level in volts (-2.0V to +2.95V)	
                term    : TERMINATION voltage level in volts (-2.0V to +3.0V)					
        """
        self.send(':SOUR:VOLT' + str(channel) + str(':LEV:IMM:HIGH ') + str(high) + 'V')
        self.send(':SOUR:VOLT' + str(channel) + str(':LEV:IMM:LOW ') + str(low)+ 'V')
        self.send(':SOUR:VOLT' + str(channel) + str(':LEV:IMM:TERM ') + str(term)+ 'V')

    def set_clock_source_internal(self):
        """
            Use internal oscillator for time-base			
        """
        self.send(':TRIG:SOUR IMM')

    def set_clock_source_reference(self):
        """
            Use external 10MHz clock source for time-base
            This clock is used for all timing parameters
        """
        self.send(':TRIG:SOUR REF')

    def enable_output(self, channel=1):
        """
            Enable positive and negative output
			
            args:
                channel : 1 or 2			
        """
        self.send(':OUTP' + str(channel) + str(':NEG ON'))
        self.send(':OUTP' + str(channel) + str(':POS ON'))

    def disable_output(self, channel=1):
        """
            Enable positive and negative output
			
            args:
                channel : 1 or 2			
        """
        self.send(':OUTP' + str(channel) + str(':NEG OFF'))
        self.send(':OUTP' + str(channel) + str(':POS OFF'))
