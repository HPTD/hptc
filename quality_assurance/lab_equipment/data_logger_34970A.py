#!/usr/bin/env python

from . visa_comm import VisaComm
import logging

# -------------------------------------------------------------
#  ----------------------- Class Scope -----------------------
# -------------------------------------------------------------

# Commands to this scope can be found under: http://www.keysight.com/upload/cmc_upload/All/9000_series_prog_ref.pdf?&cc=CH&lc=ger

class DataLogger(VisaComm):

	def __init__(self, rm_visa, str_addr, logger_file):
		VisaComm.__init__(self, rm_visa, str_addr)
		self.logger = logger_file
		self.table = {}

	def reset(self):
		self.logger.info('---- Data Logger reset')
		self.send('*RST')
		
	def get_id(self):
		stat = self.query('*IDN?')
		return stat
		
	def config_scan(self, channel):
		self.logger.info('---- Data Logger scan configuration')
		msg = 'ROUTe:SCAN (@'
		for element in channel:
			msg = msg + str(element)
			if element == channel[-1]:
				msg = msg + ')'
			else:
				msg = msg + ', '
		self.logger.debug(msg)
		self.send(msg)
	
	def config_trig_timer(self, interval, count):
		self.logger.info('---- Data Logger trigger configuration')
		count = count.upper()
	
		msg = 'TRIGger:SOURce TIMer'
		self.logger.debug(msg)
		self.send(msg)
		msg = 'TRIGger:TIMer ' + str(interval)
		self.logger.debug(msg)
		self.send(msg)
		msg = 'TRIGger:COUNt ' + count
		self.logger.debug(msg)
		self.send(msg)
		
	def config_pt(self, channel, type):
		self.logger.info('---- Data Logger channel ' + str(channel) + ' configuration')
		
		self.table[channel] = type
		msg = 'CONFigure:FRESistance (@' + str(channel) + ')'
		self.logger.debug(msg)
		self.send(msg)
	
	def start_scan(self):
		self.logger.info('---- Data Logger start scan')
		msg = 'INITiate'
		self.logger.debug(msg)
		self.send(msg)
		
	def stop_scan(self):
		self.logger.info('---- Data Logger stop scan')
		msg = 'ABORt'
		self.logger.debug(msg)
		self.send(msg)
		
	def get_count(self, channel):
		msg = 'CALCulate:AVERage:COUNt? (@' + str(channel) + ')'
		self.logger.debug(msg)
		stat = self.query(msg)
		return stat
	
	def clear_count(self):
		self.logger.info('---- Data Logger clear count')
		self.send('CALCulate:AVERage:CLEAR')
	
	def get_res(self, channel):
		self.logger.debug('---- Data Logger get channel ' + str(channel) + ' resistance')
		msg = 'CALCulate:AVERage:AVERage? (@' + str(channel) + ')'
		self.logger.debug(msg)
		stat = float(self.query(msg))
		return stat
	
	def get_temp(self, channel):
		stat = self.get_res(channel)
		
		if (self.table[channel] == 'pt1000'):
			stat = stat/10
		stat = (stat - 100) / 0.385
		return stat