#!/usr/bin/env python

import socket
import logging
import time
# -------------------------------------------------------------
#  -------------- Class DriverComm (driver-layer) ------------
# -------------------------------------------------------------

class DriverComm:

    def __init__(self, ip='192.168.1.2', port=9001, insert_newline=1, try_reconnect=0, loggername=None):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.MAX_RECONNECT_TRIALS = 3
        self.INSERT_NEW_LINE      = insert_newline
        self.TRY_RECONNECT        = try_reconnect
        self.logger = logging.getLogger(loggername)

        self.s.connect((ip, port))
        self.logger.info('Opening socket at port:' + str(port) + ", ip:" + ip)

    def send(self, data):
        data = data.encode('utf-8')
        if self.INSERT_NEW_LINE:
            if data[-1] != b'\n':
                data += b'\n'
        success = 0
        number_trial = 0
        while(not success and number_trial<=self.MAX_RECONNECT_TRIALS):
            try:
                self.s.send(data)
                success = 1
            except ConnectionAbortedError:
                self.logger.error('send: Ethernet Connection Aborted Error for device port:' + str(port) + ', ip:' + ip)
                if(self.TRY_RECONNECT):
                    number_trial = number_trial + 1
                    success = 0
                    self.logger.info('send: Trying to reconnect...')
                    self.s.connect((ip, port))
                    time.sleep(1)
                    self.logger.info('send: Opening socket at port:' + str(port) + ", ip:" + ip)
                else:
                    return 0

        return success

    def recv(self):
        r = ' '
        data = ''
        while r != '\n':
            data += self.s.recv(4 * 1024)
            r = data[-1]
        return data[:-1]

    def query(self, data):
        self.send(data)
        return self.s.recv(4096)

    # msg is a list of integers which will be converted to bytes
    def send_bytes(self, msg):
        msg_bytes = bytes(msg)
        self.s.send(msg_bytes)

    # msg is a list of integers which will be converted to bytes
    # returns list of integers
    def query_bytes(self, msg):
        self.send_bytes(msg)
        data_return =  self.s.recv(4096)
        data_ret_int = [i for i in data_return]		
        return data_ret_int