#!/usr/bin/env python

from . visa_comm import VisaComm
import time
import datetime
import os
import csv

# ---------------------------------------------------------------------------
#  ----------------------- Class Lightwave Multimeter -----------------------
# ---------------------------------------------------------------------------

# Commands to this instrument can be found under: http://research.physics.illinois.edu/bezryadin/labprotocol/Keithley2400Manual.pdf

class SourceMeter(VisaComm):
    def __init__(self, rm_visa, str_addr):
        VisaComm.__init__(self, rm_visa, str_addr)
		
    # Instrument Interface Commands
    def get_id(self):
        stat = self.query('*IDN?')
        return stat

	# Reset command
    def reset(self):
        stat = self.send('*RST')

	# Enable output
    def source_enable(self, enable_ndisable=1):
        if(enable_ndisable==0)   : str_en = 'OFF'
        elif(enable_ndisable==1) : str_en = 'ON'
        else : return 0
		
        stat = self.send(':OUTP ' + str_en)

    def dut_power_cycle(self, time_wait = 120):
        self.source_enable(0)
        time.sleep(time_wait)		
        self.source_enable(1)
        time.sleep(time_wait/4)
        self.get_current()
		
    # Inspired on 3-24
    def init_for_sfp_onu(self):
        self.send('*RST')
        self.send(':SOUR:FUNC VOLT')
        self.send(':SOUR:VOLT:MODE FIXED')
        self.send(':SOUR:VOLT:LEV:IMM:AMPL 3.3') # 3.3V
        self.send(':SENS:FUNC "CURR"')
        self.send(':SENS:CURR:RANG:AUTO ON')
        self.send(':SENS:CURR:PROT 0.8')         #0.8A
        self.send(':SYST:RSENSE 1')              #4W sensing

    #Return current in Amperes
    def get_current(self):
        return float(((self.query(':READ?')).split(','))[1])

    def save_current(self, name_file, iteration=0):
        # Create Formatted Vector
        fieldnames = ['%20s' % 'iteration','%20s' % 'date','%20s' % 'time', '%20s' % 'current_A']
        current = self.get_current()
        date_time = datetime.datetime.now()
        measurements = { '%20s' % 'iteration' : '%20d' % iteration,
                         '%20s' % 'date'      : '%20s' % date_time.date(),
                         '%20s' % 'time'      : '%20s' % date_time.time(),						 
                         '%20s' % 'current_A' : '%20.10f' % current}

        # Save Measurement
        # Check if file exists 
        file_exists = os.path.isfile(name_file + '.csv')  
        with open(name_file + '.csv', 'a') as csvfile:
            writer = csv.DictWriter(
                csvfile,
                fieldnames=fieldnames,
                delimiter=',',
                lineterminator='\n')
            if (not file_exists) : writer.writeheader()
            writer.writerow(measurements)

        return current