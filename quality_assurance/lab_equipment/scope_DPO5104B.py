#!/usr/bin/env python

from . visa_comm import VisaComm
import logging

# -------------------------------------------------------------
#  ----------------------- Class Scope -----------------------
# -------------------------------------------------------------

# Commands to this scope can be found under: http://www.keysight.com/upload/cmc_upload/All/9000_series_prog_ref.pdf?&cc=CH&lc=ger

class Scope(VisaComm):

	def __init__(self, rm_visa, str_addr, logger_file):
		VisaComm.__init__(self, rm_visa, str_addr)
		self.logger = logger_file

	def get_id(self):
		stat = self.query('*IDN?')
		return stat

	def reset(self):
		self.logger.info('---- Oscilloscope reset')
		self.send('*CLS')
		self.send('*RST')
	
	def auto_set(self):
		self.logger.info('---- Oscilloscope auto set')
		self.send('AUTOSet EXECute')
		
	def header_off(self):
		self.send('HEADER OFF')
		
	def stats_count_reset(self):
		self.send('MEASUREMENT:STATIstics:COUNt RESET')
		
	def get_count(self, measure):
		msg = 'MEASUREMENT:MEAS' + str(measure) + ':COUNt?'
		stat = self.query(msg)
		return stat
		
	def config_channel(self, channel, display, name, couplage, termination, scale, position):
		channel = channel.upper()
		couplage = couplage.upper()
		termination = termination.upper()
		scale = scale.upper()
		if (isinstance(position, int)):
			position = float(position)
		position = str('%03.2f' % position)
		self.logger.info('---- Oscilloscope Channel ' + channel + ' configuration')
		
		msg = ':SELECT:' + channel + ' '
		if display == 1:
			msg = msg + 'ON'
		else:
			msg = msg + 'OFF'
		self.logger.debug(msg)
		self.send(msg)
		msg = channel + ':LABEL:NAMe "' + name + '"'
		self.logger.debug(msg)
		self.send(msg)
		msg = ':' + channel + ':COUPling ' + couplage
		self.logger.debug(msg)
		self.send(msg)
		msg = ':' + channel + ':TERmination ' + termination
		self.logger.debug(msg)
		self.send(msg)
		msg = channel + ':SCAle ' + scale
		self.logger.debug(msg)
		self.send(msg)
		msg = ':' + channel + ':POSition ' + position
		self.logger.debug(msg)
		self.send(msg)
		
	def config_trig(self, channel, mode, type, source, logic, level):
		channel = channel.upper()
		mode = mode.upper()
		type = type.upper()
		source = source.upper()
		logic = logic.upper()
		level = level.upper()
		self.logger.info('---- Oscilloscope Trigger ' + channel + ' configuration')
		
		msg = 'TRIGger:' + channel + ':MODe ' + mode
		self.logger.debug(msg)
		self.send(msg)
		msg = 'TRIGger:' + channel + ':TYPe ' + type
		self.logger.debug(msg)
		self.send(msg)
		msg = 'TRIGger:' + channel + ':LOGIc:INPut:' + source + ' ' + logic
		self.logger.debug(msg)
		self.send(msg)
		msg = 'TRIGger:' + channel + ':LEVel ' + level
		self.logger.debug(msg)
		self.send(msg)
		
	def config_display(self, unit, persistence, sample):
		self.logger.info('---- Oscilloscope Display configuration')
		unit = unit.upper()
		sample = sample.upper()
		
		msg = 'HORizontal:MODE:SAMPLERate ' + sample
		self.logger.debug(msg)
		self.send(msg)
		msg = 'HORizontal:SCAle ' + unit
		self.logger.debug(msg)
		self.send(msg)
		msg = 'DISplay:PERsistence ' + persistence
		self.logger.debug(msg)
		self.send(msg)
		
	def config_measure(self, channel, state, type, unit, s1, s2, sigtype1, sigtype2, edge1, edge2, dir):
		type = type.upper()
		s1 = s1.upper()
		s2 = s2.upper()
		sigtype1 = sigtype1.upper()
		sigtype2 = sigtype2.upper()
		edge1 = edge1.upper()
		edge2 = edge2.upper()
		dir = dir.upper()
		self.logger.info('---- Oscilloscope Measure ' + str(channel) + ' configuration')
		
		msg = 'MEASUREMENT:MEAS' + str(channel) + ':STATE ' + str(state)
		self.logger.debug(msg)
		self.send(msg)
		msg = 'MEASUREMENT:MEAS' + str(channel) + ':TYPE ' + type
		self.logger.debug(msg)
		self.send(msg)
#		self.logger.debug(msg)
#		self.send(msg)
		msg = 'MEASUREMENT:MEAS' + str(channel) + ':UNITS "' + unit + '"'
		self.logger.debug(msg)
		self.send(msg)
		msg = 'MEASUREMENT:MEAS' + str(channel) + ':SOURCE1 ' + s1
		self.logger.debug(msg)
		self.send(msg)
		msg = 'MEASUREMENT:MEAS' + str(channel) + ':SOURCE2 ' + s2
		self.logger.debug(msg)
		self.send(msg)
		msg = 'MEASUREMENT:MEAS' + str(channel) + ':SOURCE1:SIGTYPE ' + sigtype1
		self.logger.debug(msg)
		self.send(msg)
		msg = 'MEASUREMENT:MEAS' + str(channel) + ':SOURCE2:SIGTYPE ' + sigtype2
		self.logger.debug(msg)
		self.send(msg)
		msg = 'MEASUREMENT:MEAS' + str(channel) + ':EDGE1 ' + edge1
		self.logger.debug(msg)
		self.send(msg)
		msg = 'MEASUREMENT:MEAS' + str(channel) + ':EDGE2 ' + edge2
		self.logger.debug(msg)
		self.send(msg)
		msg = 'MEASUREMENT:MEAS' + str(channel) + ':DELay:DIRECTION ' + dir
		self.logger.debug(msg)
		self.send(msg)
		msg = 'MEASUREMENT:MEAS' + str(channel) + ':REFLEVEL:METHOD PERCent'
		self.logger.debug(msg)
		self.send(msg)
		msg = 'MEASUREMENT:MEAS' + str(channel) + ':REFLEVEL:METHOD:PERCENT:HIGH 90'
		self.logger.debug(msg)
		self.send(msg)
		msg = 'MEASUREMENT:MEAS' + str(channel) + ':REFLEVEL:METHOD:PERCENT:LOW 10'
		self.logger.debug(msg)
		self.send(msg)
		msg = 'MEASUREMENT:MEAS' + str(channel) + ':REFLEVEL:METHOD:PERCENT:MID1 40'
		self.logger.debug(msg)
		self.send(msg)
		msg = 'MEASUREMENT:MEAS' + str(channel) + ':REFLEVEL:METHOD:PERCENT:MID2 60'
		self.logger.debug(msg)
		self.send(msg)
	
	def get_measure_min(self, meas):
		self.logger.debug('---- Oscilloscope get MIN measure ' + str(meas))
		msg = 'MEASUrement:MEAS' + str(meas) + ':MINImum?'
		self.logger.debug(msg)
		stat = self.query(msg)
		return float(stat)
		
	def get_measure_mean(self, meas):
		self.logger.debug('---- Oscilloscope get MEAN measure ' + str(meas))
		msg = 'MEASUrement:MEAS' + str(meas) + ':MEAN?'
		self.logger.debug(msg)
		stat = self.query(msg)
		return float(stat)
		
	def get_measure_max(self, meas):
		self.logger.debug('---- Oscilloscope get MAX measure ' + str(meas))
		msg = 'MEASUrement:MEAS' + str(meas) + ':MAXimum?'
		self.logger.debug(msg)
		stat = self.query(msg)
		return float(stat)
		
	def get_measure_stdev(self, meas):
		self.logger.debug('---- Oscilloscope get STDEV measure ' + str(meas))
		msg = 'MEASUrement:MEAS' + str(meas) + ':STDdev?'
		self.logger.debug(msg)
		stat = self.query(msg)
		return float(stat)
		
	def record_length(self, length):
		length = length.upper()
		msg = 'HORizontal:RECOrdlength ' + length
		self.logger.debug(msg)
		self.send(msg)
		
	def config_zoom(self, mode, position, scale):
		self.logger.info('---- Oscilloscope Zoom configuration')
		scale = scale.upper()
		
		msg = 'ZOOM:MODe ' + str(mode)
		self.logger.debug(msg)
		self.send(msg)
		msg = 'ZOOM:CH1:HORizontal:POSition ' + str(position)
		self.logger.debug(msg)
		self.send(msg)
		msg = 'ZOOM:CH1:HORizontal:SCAle ' + str(scale)
		self.logger.debug(msg)
		self.send(msg)

