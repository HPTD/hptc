#!/usr/bin/env python

from . driver_comm import DriverComm
from . sfp_sff8472 import SFP_SFF8472

import logging
import math
import csv
import os
import datetime

# -------------------------------------------------------------
#  -------- Class EthI2C (Ethernet <-> i2c control) -----------
# -------------------------------------------------------------

# Class implemented by EBSM in order to control Ethernet to IIC box developed by Christophe Sigaud (CHS)
# OBS : slv_addr always assume a 7-bit addressing scheme
# OBS2: inherits from SFP_SFF8472 in order to have SFP diagnostic functionality
class EthI2C(DriverComm, SFP_SFF8472):

    def __init__(self, ip='192.168.1.100', port=5000):
        DriverComm.__init__(self, ip, port)
        SFP_SFF8472.__init__(self)
        # Switch configuration to change port addresses
        self.PORT_SW2=1
        self.PORT_SW3=1
        self.PORT_SW4=1
        self.PORT_SW1=1

    # ----------------------- Methods generic for the Eth2IIC IC used by CHS ---------------------------------
    # data list is a list of integers
    # returns 1=success, 0=failed (displays warning in self.logger when failed)
    def i2c_write(self,slv_addr, data_list):
        # Format command according to LANIICMSSock documentation	
        CMD = 0x0120
        ucSlvAddr = slv_addr << 1
        RES = 0x00
        wSize = len(data_list)
        wdata = data_list

        # Format message
        msg = []		
        msg.append(CMD        & 0xFF)
        msg.append(CMD>>8     & 0xFF)
        msg.append(ucSlvAddr  & 0xFF)
        msg.append(RES        & 0xFF)
        msg.append(wSize      & 0xFF)
        msg.append((wSize>>8) & 0xFF)
        for i in range(0, wSize):
            msg.append(wdata[i] & 0xFF)

        # Send message and get return value
        data_ret     = self.query_bytes(msg)

        # Format return value according to LANIICMSSock documentation
        Packet_Length_ret = (data_ret[1]<<8) + data_ret[0]			
        CMD_ret           = (data_ret[3]<<8) + data_ret[2]		
        ucSlvAddr_ret     = data_ret[4]
        RES_ret           = data_ret[5]
        wStatus_ret       = (data_ret[7]<<8) + data_ret[6]
        wSize_ret         = (data_ret[9]<<8) + data_ret[8]

        # Check information correctness
        success = 1
        Packet_Length = 0x000A
        if(Packet_Length_ret != Packet_Length):
            self.logger.warn('I2C write operation, field Packet Length returned incorrect. Packet Length=' + str(Packet_Length) + ', ret='+ str(Packet_Length_ret))
            success = 0

        if(CMD_ret != CMD):
            self.logger.warn('I2C write operation, field CMD returned incorrect. CMD=' + str(CMD) + ', ret='+ str(CMD_ret))
            success = 0

        if(ucSlvAddr_ret != ucSlvAddr):
            self.logger.warn('I2C write operation, field ucSlvAddr returned incorrect. ucSlvAddr=' + str(ucSlvAddr) + ', ret='+ str(ucSlvAddr_ret))
            success = 0

        if(RES_ret != RES):
            self.logger.warn('I2C write operation, field RES returned incorrect. RES=' + str(RES) + ', ret='+ str(RES_ret))
            success = 0

        #wStatus = 
        #if(wStatus_ret != wStatus):
        #    self.logger.warn('I2C write operation, field wStatus returned incorrect. wStatus=' + str(wStatus) + ', ret='+ str(wStatus_ret))
        #    success = 0

        if(wSize_ret != wSize):
            self.logger.warn('I2C write operation, field wSize returned incorrect. wSize=' + str(wSize) + ', ret='+ str(wSize_ret))
            success = 0

        return success


    # data list is a list of integers
    # returns a list where the first element indicates success (0,1) and the others are the read data values
    def i2c_read(self,slv_addr, nbr_read):
        # Format command according to LANIICMSSock documentation	
        CMD = 0x0220
        ucSlvAddr = slv_addr << 1
        RES = 0x00
        wSize = nbr_read

        # Format message
        msg = []		
        msg.append(CMD        & 0xFF)
        msg.append(CMD>>8     & 0xFF)
        msg.append(ucSlvAddr  & 0xFF)
        msg.append(RES        & 0xFF)
        msg.append(wSize      & 0xFF)
        msg.append((wSize>>8) & 0xFF)

        # Send message and get return value
        data_ret     = self.query_bytes(msg)

        # Format return value according to LANIICMSSock documentation
        Packet_Length_ret = (data_ret[1]<<8) + data_ret[0]			
        CMD_ret           = (data_ret[3]<<8) + data_ret[2]		
        ucSlvAddr_ret     = data_ret[4]
        RES_ret           = data_ret[5]
        wStatus_ret       = (data_ret[7]<<8) + data_ret[6]
        wSize_ret         = (data_ret[9]<<8) + data_ret[8]

        rdata = []
        for i in range(10,len(data_ret)):
            rdata.append(data_ret[i])

        # Check information correctness
        success = 1
        if(CMD_ret != CMD):
            self.logger.warn('I2C read operation, field CMD returned incorrect. CMD=' + str(CMD) + ', ret='+ str(CMD_ret))
            success = 0

        if(ucSlvAddr_ret != ucSlvAddr):
            self.logger.warn('I2C read operation, field ucSlvAddr returned incorrect. ucSlvAddr=' + str(ucSlvAddr) + ', ret='+ str(ucSlvAddr_ret))
            success = 0

        if(RES_ret != RES):
            self.logger.warn('I2C read operation, field RES returned incorrect. RES=' + str(RES) + ', ret='+ str(RES_ret))
            success = 0

        #wStatus = 
        #if(wStatus_ret != wStatus):
        #    self.logger.warn('I2C read operation, field wStatus returned incorrect. wStatus=' + str(wStatus) + ', ret='+ str(wStatus_ret))
        #    success = 0

        if(wSize_ret != wSize):
            self.logger.warn('I2C read operation, field wSize returned incorrect. wSize=' + str(wSize) + ', ret='+ str(wSize_ret))
            success = 0

        rdata[0] = success
        return rdata
	
    # returns a list where the first element indicates success (0,1) and the others are the read data values
    def i2c_writeread(self,slv_addr, nbr_read, data_list):
        # Format command according to LANIICMSSock documentation	
        CMD = 0x0820
        ucSlvAddr = slv_addr << 1
        RES = 0x00
        wwrSize = len(data_list)
        wrdSize = nbr_read      		
        wdata = data_list

        # Format message
        msg = []		
        msg.append(CMD        & 0xFF)
        msg.append(CMD>>8     & 0xFF)
        msg.append(ucSlvAddr  & 0xFF)
        msg.append(RES        & 0xFF)
        msg.append(wwrSize    & 0xFF)
        msg.append((wwrSize>>8) & 0xFF)
        msg.append(wrdSize    & 0xFF)
        msg.append((wrdSize>>8) & 0xFF)
        for i in range(0, wwrSize):
            msg.append(wdata[i] & 0xFF)

        # Send message and get return value
        data_ret     = self.query_bytes(msg)

        # Format return value according to LANIICMSSock documentation
        Packet_Length_ret = (data_ret[1]<<8) + data_ret[0]			
        CMD_ret           = (data_ret[3]<<8) + data_ret[2]		
        ucSlvAddr_ret     = data_ret[4]
        RES_ret           = data_ret[5]
        wStatus_ret       = (data_ret[7]<<8) + data_ret[6]
        wrdSize_ret         = (data_ret[9]<<8) + data_ret[8]

        rdata = [1]
        for i in range(10,len(data_ret)):
            rdata.append(data_ret[i])

        # Check information correctness
        success = 1
        if(CMD_ret != CMD):
            self.logger.warn('I2C read_mem operation, field CMD returned incorrect. CMD=' + str(CMD) + ', ret='+ str(CMD_ret))
            success = 0

        if(ucSlvAddr_ret != ucSlvAddr):
            self.logger.warn('I2C read_mem operation, field ucSlvAddr returned incorrect. ucSlvAddr=' + str(ucSlvAddr) + ', ret='+ str(ucSlvAddr_ret))
            success = 0

        if(RES_ret != RES):
            self.logger.warn('I2C read_mem operation, field RES returned incorrect. RES=' + str(RES) + ', ret='+ str(RES_ret))
            success = 0

        #wStatus = 
        #if(wStatus_ret != wStatus):
        #    self.logger.warn('I2C read_mem operation, field wStatus returned incorrect. wStatus=' + str(wStatus) + ', ret='+ str(wStatus_ret))
        #    success = 0

        if(wrdSize_ret != wrdSize):
            self.logger.warn('I2C read_mem operation, field wrdSize returned incorrect. wrdSize=' + str(wrdSize) + ', ret='+ str(wrdSize_ret))
            success = 0

        rdata[0] = success
        return rdata

    # 500< Frequency in Hz <1e6; 0<duty_cycle<1
    def i2c_set_freq(self, frequency, duty_cycle=0.5):
        # Find low and high coefficients for SCL (clk iic)
        high  = duty_cycle*6e7/(frequency)
        low   = high*(1-duty_cycle)/(duty_cycle) 

        # Format command according to LANIICMSSock documentation	
        CMD = 0x0320
        wSCLH = int(high)
        wSCLL = int(low)

        # Format message
        msg = []		
        msg.append(CMD        & 0xFF)
        msg.append(CMD>>8     & 0xFF)
        msg.append(wSCLH      & 0xFF)
        msg.append((wSCLH>>8) & 0xFF)
        msg.append(wSCLL      & 0xFF)
        msg.append((wSCLL>>8) & 0xFF)

        # Send message and get return value
        data_ret     = self.query_bytes(msg)

        # Format return value according to LANIICMSSock documentation
        Packet_Length_ret = (data_ret[1]<<8) + data_ret[0]			
        CMD_ret           = (data_ret[3]<<8) + data_ret[2]
        wSCLH_ret         = (data_ret[5]<<8) + data_ret[4]
        wSCLL_ret         = (data_ret[7]<<8) + data_ret[6]

        # Check information correctness
        success = 1
        Packet_Length = 0x0008
        if(Packet_Length_ret != Packet_Length):
            self.logger.warn('I2C set SCL freq operation, field Packet Length returned incorrect. Packet Length=' + str(Packet_Length) + ', ret='+ str(Packet_Length_ret))
            success = 0

        if(CMD_ret != CMD):
            self.logger.warn('I2C set SCL freq operation, field CMD returned incorrect. CMD=' + str(CMD) + ', ret='+ str(CMD_ret))
            success = 0

        if(wSCLH_ret != wSCLH):
            self.logger.warn('I2C set SCL freq operation, field wSCLH returned incorrect. wSCLH=' + str(wSCLH) + ', ret='+ str(wSCLH_ret))
            success = 0

        if(wSCLL_ret != wSCLL):
            self.logger.warn('I2C set SCL freq operation, field wSCLL returned incorrect. wSCLL=' + str(wSCLL) + ', ret='+ str(wSCLL_ret))
            success = 0

        return success
		
    # returns a list where the first element is success, second is frequency (Hz) and third is duty cycle
    def i2c_get_freq(self):

        # Format command according to LANIICMSSock documentation	
        CMD = 0x0420

        # Format message
        msg = []		
        msg.append(CMD        & 0xFF)
        msg.append(CMD>>8     & 0xFF)

        # Send message and get return value
        data_ret     = self.query_bytes(msg)

        # Format return value according to LANIICMSSock documentation
        Packet_Length_ret = (data_ret[1]<<8) + data_ret[0]			
        CMD_ret           = (data_ret[3]<<8) + data_ret[2]
        wSCLH_ret         = (data_ret[5]<<8) + data_ret[4]
        wSCLL_ret         = (data_ret[7]<<8) + data_ret[6]

        # Check information correctness
        success = 1
        Packet_Length = 0x0008
        if(Packet_Length_ret != Packet_Length):
            self.logger.warn('I2C set SCL freq operation, field Packet Length returned incorrect. Packet Length=' + str(Packet_Length) + ', ret='+ str(Packet_Length_ret))
            success = 0

        if(CMD_ret != CMD):
            self.logger.warn('I2C set SCL freq operation, field CMD returned incorrect. CMD=' + str(CMD) + ', ret='+ str(CMD_ret))
            success = 0

        frequency = 6e7/(wSCLH_ret+wSCLL_ret)
        duty_cycle = wSCLH_ret/(wSCLH_ret+wSCLL_ret)	
        return [success, frequency, duty_cycle]

    # ----------------------- Methods which are specific to the board designed by CHS ---------------------------------
    # port_number = 1-4
    # slave_channel = 0-7
    def select_channel(self, slave_channel=0, port_number=1):
	
        if(port_number==1):
            port_addr=(0xE0 + (self.PORT_SW2<<3)) >> 1
        elif(port_number==2):
            port_addr=(0xE2 + (self.PORT_SW3<<3)) >> 1
        elif(port_number==3):
            port_addr=(0xE4 + (self.PORT_SW4<<3)) >> 1
        elif(port_number==4):
            port_addr=(0xE6 + (self.PORT_SW1<<3)) >> 1

        success = self.i2c_write(port_addr, [2**slave_channel])
        return success