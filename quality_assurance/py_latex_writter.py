#!/usr/bin/env python

class PyLatexWritter():
    def __init__(self, filename):
        self.filename=filename

    def write_to_file(self, str_aux):
        file = open(self.filename, 'a')
        file.write('\n')			
        file.write(str_aux)	
        file.write('\n')			
        file.close()

    def create_section(self, name):
        file = open(self.filename, 'a')
        file.write('\n')			
        file.write('  \\section{' + name + '}\n')
        file.write('\n')			
        file.close()

    def create_subsection(self, name, include_label=0, label=''):
        file = open(self.filename, 'a')
        file.write('\n')			
        file.write('  \\subsection{' + name + '}\n')
        if(include_label) : file.write('  \\label{subsec:' + label + '}\n')		
        file.write('\n')			
        file.close()

    def create_subsubsection(self, name):
        file = open(self.filename, 'a')
        file.write('\n')		
        file.write('  \\subsubsection{' + name + '}\n')
        file.write('\n')		
        file.close()

    def clear_page(self):
        file = open(self.filename, 'a')
        file.write('\n')		
        file.write('  \\clearpage\n')
        file.write('\n')		
        file.close()

    def end_document(self):
        file = open(self.filename, 'a')
        file.write('\n')		
        file.write('\\end{document}\n')
        file.write('\n')		
        file.close()

    # space is in cm, it is a 'n'-length list
    # color is a list of string with 'm'-length corresponding to the row color (e.g. ['blue', 'red', 'green', 'gray', ...])
    # matrix is a 'm x n'-list matrix composed of strings	
    def write_table(self, space, color, matrix, caption, label):
        # open file and create table
        file = open(self.filename, 'a') # by default, open in append mode
        file.write('\n')		
        file.write('  \\begin {table}[ht] \\footnotesize\n')
        file.write('    \\begin {center}\n')

        # space for each line
        str_spacing = '    \\begin {tabular}{|'
        for i in range(0,len(space)) : str_spacing=str_spacing + 'p{'+ str(space[i]) +'cm}|'
        str_spacing=str_spacing + '}\n'
        file.write(str_spacing)
        file.write('    \\hline\n')

        # write data
        for i in range(0,len(matrix)):
            str_line = '    '
            for j in range(0,len(matrix[i])):
                str_line = str_line + '\\cellcolor{' + color[i] + '!25}' + matrix[i][j]
                if(j != len(matrix[i])-1) : str_line = str_line + ' & '
                else                      : str_line = str_line + ' \\\\ \\hline \n'
            file.write(str_line)

        # write end of table
        file.write('    \\end{tabular}\n')
        file.write('    \\end{center}\n')
        file.write('    \\caption{' + caption + '}\n')
        file.write('    \\label{table:' + label + '}\n')
        file.write('  \\end{table}\n')
        file.write('\n')	

        # close file
        file.close()

    # - width is in cm
    # - file_path points to a .png format (do not include .png in the name)
    def write_figure(self, width, file_path, caption, label):
        # open file and create figure
        file = open(self.filename, 'a')
        file.write('\n')
        file.write('  \\begin {figure}[ht]\n')
        file.write('    \\centering\n')
        file.write('    \\includegraphics[width=' + str(width) +'cm]{{' + file_path + '}.png}\n')
        file.write('    \\caption{' + caption + '}\n')
        file.write('    \\label{fig:' + label + '}\n')
        file.write('  \\end{figure}\n')
        file.write('\n')
		# close file
        file.close()

    # - width is in cm
    # - file_path_list points to a .png format (do not include .png in the name)
    #    it is a list containing the paths for all the subfig. in fig.
	# - m=row, n=column	
    # - sub_caption_list has same size as file_path
    # - sub_label_list has same size as file_path
    # - caption is the main figure caption
    # - label is the main figure label
    def write_subfigures(self, width, file_path_list, m, n, sub_caption_list, sub_label_list, caption, label):
        # open file and create figure
        file = open(self.filename, 'a')
        file.write('\n')
        file.write('  \\begin {figure}[ht]\n')

        # write subfigures
        for i in range(0,m):
            for j in range(0,n):		
                idx = i*n + j
                if(idx<len(file_path_list)):
                    file.write('    \\begin {subfigure}{' + str(1.0/float(n)) + '\\textwidth}\n')
                    file.write('      \\centering\n')
                    file.write('      \\includegraphics[width=' + str(width) +'cm]{{' + file_path_list[idx] + '}.png}\n')
                    file.write('      \\caption{' + sub_caption_list[idx] + '}\n')
                    file.write('      \\label{fig:' + sub_label_list[idx] + '}\n')
                    file.write('    \\end{subfigure}\n')
            file.write('\n')
        # end figure
        file.write('    \\caption{' + caption + '}\n')
        file.write('    \\label{fig:' + label + '}\n')
        file.write('  \\end{figure}\n')
        file.write('\n')
        # close file
        file.close()

    def __del__(self):
        pass