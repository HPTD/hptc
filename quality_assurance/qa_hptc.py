#!/usr/bin/env python

####################################################
###              Python Packages                 ###
####################################################
import logging
import visa
import time
import datetime
import matplotlib.pyplot as plt
import sys
import os

####################################################
###                 lab. equipment               ###
####################################################
from lab_equipment.visa_comm                 import VisaComm
from lab_equipment.scope_DSA91204A           import Scope
from lab_equipment.phase_noise_analyzer_FSWP import PhaseNoiseAnalyzer
from lab_equipment.PLL_Si534xEVB             import PLL_Si534xEVB
from plots import plot_all

OCXO_REFERENCE = '160269656202' #'151242626193'
AUTHOR_TESTER  = 'eduardo mendes'
name_config    = 'hptc_configdefault_lvds'

def acquire_power(power, name_file, comment='',nacq = 1):
    n=0
    while(n < nacq):
        cur       = float(power.query('MEAS:Current?'))
        volt      = float(power.query('MEAS:Voltage?'))
        date_time = datetime.datetime.now()
        if(n==0) : comment_wr = comment
        else     : comment_wr = ''
        with open(name_file, 'a') as f:
            f.write('%20s,%20s,%20s,%20.10f,%20.10f\n' % (comment_wr,date_time.date(), date_time.time(),cur,volt))
        n=n+1
        time.sleep(1)

def main(power, scope, PNA, PLL):

    ####################################################
    ###            Application parameters            ###
    ####################################################	
    # PNA PARAMETERS	
    FREQ_START_OFFSET_Hz = 1    # start frequency offset with respect to carrier
    FREQ_STOP_OFFSET_Hz  = 10e6 # stop frequency offset with respect to carrier
    NUMBER_AVERAGING     = 1    # average N measurements
    CORRELATION_FACTOR   = 10   # cross-correlation for measurements
    BDW_RESOLUTION       = 3    # bandwidth resolution in percentage
    
    ####################################################
    ###                Test routine                  ###
    ####################################################
    logger_test_log.info(' ')
    logger_test_log.info(' -------------------------------------------------')
    logger_test_log.info(' ---------- HPTC power-up application ------------' )
    logger_test_log.info(' -------------------------------------------------')
    
    # Setup the power supply 
    power.send('INST:NSEL 1') # Output 1
    power.send('SOUR:VOLT 5') # 5V
    power.send('SOUR:CURR 3') # 3A max
    
    ROOT_PATH   = './results/hptc_' + OCXO_REFERENCE
    create_dir = 1    
    for folders in os.listdir('.'):
        if('hptc_'+OCXO_REFERENCE in folders) : create_dir = 0
    if create_dir : os.mkdir(ROOT_PATH)
    
    with open(ROOT_PATH + '/power.txt', 'w') as f:
        f.write('%20s,%20s,%20s,%20s,%20s\n' % ('Comment','Date', 'Time', 'Current(A)', 'Voltage(V)'))
    
    logger_test_log.info(' ')
    logger_test_log.info(' -------------------------------------------------')
    logger_test_log.info(' ----------------- Power-up ----------------------')
    logger_test_log.info(' -------------------------------------------------')
    # Disable output
    power.send('OUTP:STAT 0')
    logger_test_log.info('Output disabled, reading voltage+current for 10s...')
    
    # Initial 10 acquisitions without powering    
    logger_test_log.info('Output off, reading voltage+current for 10s...')
    acquire_power(power, ROOT_PATH + '/power.txt', 'setup off', 10)
    
    # Enable output
    power.send('OUTP:STAT 1')
    logger_test_log.info('Output enabled, reading voltage+current for 10min...')
    
    # 10min acquisitions - 10*60 acquisitions with powering   
    acquire_power(power, ROOT_PATH + '/power.txt', 'power-up', 10*60)
    
    # Configure HPTC
    logger_test_log.info(' ')
    logger_test_log.info(' -------------------------------------------------')
    logger_test_log.info(' ---------------- Configure si5344 ---------------')
    logger_test_log.info(' -------------------------------------------------')
    logger_test_log.info('PLL configuration: ' + name_config)
    if(not ('nvm' in name_config) ): PLL.write_full_config('./' + name_config + '.slabtimeproj', 0)
    else                         : logger_test_log.info('... PLL not configured as NVM should have been already programmed')
    logger_test_log.info(' ')
    logger_test_log.info(' -------------------------------------------------')
    logger_test_log.info(' ------------------- Setup scope -----------------')
    logger_test_log.info(' -------------------------------------------------')
    scope.send('*RST')
    scope.trig_stop()
    scope.send(':ACQuire:SRATe:ANALog 40e9')   #Sampling-rate
    scope.send(':ACQuire:POINts:ANALog 100e6') # 100e6 points, corresponding to a 2.5ms window
    
    # Display only channel 2    
    scope.send(':CHAN2:SCAL 75e-3')      #Vertical scale 75mV/div
    scope.send(':CHAN4:SCAL 75e-3')      #Vertical scale 75mV/div
    scope.send(':CHAN1:DISP OFF')
    scope.send(':CHAN2:DISP ON')
    scope.send(':CHAN3:DISP OFF')
    scope.send(':CHAN4:DISP OFF')
    scope.send(':CHAN2:DIFF ON')               #Differential 2-4
    scope.send(':CHAN2:DISP:SCAL 150e-3')      #Vertical scale 150mV/div
    
    # Measurements
    scope.send(':MEAS:TIEClock2 CHAN2,SEC,RIS,AUTO')
    scope.send(':HIST:MODE MEAS\n')
    scope.send(':MEAS:FALL CHAN2')
    scope.send(':MEAS:RIS  CHAN2')
    scope.send(':MEAS:DUTY CHAN2')
    scope.send(':MEAS:VAMP CHAN2')
    
    scope.send(':MEAS:THResholds:RFAL:METH ALL,PERC')
    scope.send(':MEASure:THResholds:RFALl:PERCent ALL,80,50,20')
    
    # Trigger
    scope.send(':TRIGger:EDGE:SOURce CHAN2')
    
    logger_test_log.info(' -------------------------------------------------')
    
    for output in range(0,4):
        NAME_PATH   = ROOT_PATH + '/T' + str(output)
        os.mkdir(NAME_PATH)
        a='n'
        b='n'
        while(a!= 'y' or b!='y'):
            print('- Ready to perform a new measurement')
            a = input('-> Is OUT' + str(output) + ' connected to PNA (y)?')
            b = input('-> Is OUT' + str((output+1)%4) + ' connected to scope (y)?')
            if(a!= 'y' or b!='y'):
                print('- Make the appropriate connections before continuing...')
    
        # Save power just for info
        acquire_power(power, ROOT_PATH + '/power.txt', 'T' + str(output), 1)
    
        ####################################################
        ###                    Scope                     ###
        ####################################################
        logger_test_log.info(' --- (1) Start Scope measurement' )
        scope.display_clear()
        scope.get_acq_done()
        done = scope.get_acq_done()
        scope.trig_sing()
        while(not done):
            done = scope.get_acq_done()
            time.sleep(1)
        time.sleep(2)
    
        scope.save_measurement(NAME_PATH + '/SCOPE_measurements')
        hist_data = scope.histogram_save(NAME_PATH + '/SCOPE_hist.txt')
        y = hist_data[1]
        [xorigin, hist_res, hist_mean,hist_median, hist_pp,hist_min,hist_max, hist_rms, hits] = hist_data[0]
        x=[]
        for auxxx in range(0,len(y)):
            x.append(xorigin*1e12+hist_res*1e12*auxxx-hist_mean*1e12)		
        plt.plot(x,y, 'r', label='Hist / rms=%4.2f ps' % (hist_rms*1e12))
        plt.legend()
        plt.xlabel('TIE (ps)')        
        plt.ylabel('Probability density function')  
        plt.savefig(NAME_PATH + '/SCOPE_hist.png')
        plt.close()
    
        # Measurement itself
        logger_test_log.info(' --- (2) Start Phase Noise Measurement' )
        
        # PNA setup for measurement
        ####################################################
        ###                   Init PNA                   ###
        ####################################################
        # PNA setup for measurement
        PNA.preset()
        PNA.cls()
        PNA.set_phase_noise_measurement()
        PNA.set_automatic_frequency_search()
        PNA.set_offset(FREQ_START_OFFSET_Hz, FREQ_STOP_OFFSET_Hz)
        PNA.set_averaging(NUMBER_AVERAGING)
        PNA.set_correlation(CORRELATION_FACTOR)
        PNA.set_bdw_resolution(BDW_RESOLUTION)
        PNA.set_trace(trace_nbr=1, mode='AVER', label_on=1, label_str=OCXO_REFERENCE, result='PN',
                  smoothing_en=0, smoothing_pct=1, spur_remove_en=0, spur_remove_thr=6)
        PNA.set_trace(trace_nbr=2, mode='AVER', label_on=1, label_str=OCXO_REFERENCE+' - smooth', result='PN',
                  smoothing_en=1, smoothing_pct=1, spur_remove_en=1, spur_remove_thr=6)
        PNA.set_pn_analysis_range(1, 1, 1, 1, 10e6)
        
        ####################################################
        ###              Run Measurement PNA             ###
        ####################################################
        # Run measurement and check ERROR status
        measurement_status = PNA.run_measurement_routine()
        if(measurement_status[0]):  # Measurement worked
            # Append information to logger
            logger_test_log.info('Measurement finished successfully in PNA, saving results ...')
        
            # Save screen of the PNA to our PC
            PNA.save_screen(NAME_PATH + '/pna_screen.png')
        
            # Save phase noise to file
            [freq, phase_noise] = PNA.get_phase_noise_data(1)
            PNA.save_trace(1,NAME_PATH + '/PNA_tr1_clock.csv',
                             NAME_PATH + '/PNA_tr1_spur_clock.csv')
            PNA.save_trace(2,NAME_PATH + '/PNA_tr2_clock.csv',
                             NAME_PATH + '/PNA_tr2_spur_clock.csv')
            data = PNA.get_pn_analysis_data()
            integrated_phase_noise = data[1]
        
            # Append information to logger
            logger_test_log.info('Measurement finished successfully in PNA, saving results ...')
        
            # Perform a dynamic plot
            plt.figure(figsize=(8,6))
            plt.semilogx(freq, phase_noise, color='b', label=r'$\sigma$' + (' (1Hz-10MHz): %4d fs rms' % (integrated_phase_noise*1e15)))
            plt.xlabel('Frequency offset (Hz)')
            plt.ylabel('Phase noise (dBc/Hz)')
            plt.title('HPTC ' + OCXO_REFERENCE + ' - T'+ str(output) + 'MHz' )
            plt.grid(True)
            plt.legend(fontsize="medium")
            plt.pause(0.1)
            plt.savefig(NAME_PATH + '/PNA.png')
            plt.close()
        else:  #Measurement failed
            logger_test_log.error('Measurement error - ' + measurement_status[1] + ' - no results will be saved')
            time.sleep(5)
        # Measurement in PNAfinished
    
    logger_test_log.info('Measurements finished, creating plots...')
    os.mkdir(ROOT_PATH+'/img')
    plot_all(ROOT_PATH)

    # Create a .tex report
    with open('template_tests.tex', 'r') as f:
        data = f.read()
    file_path =  './results/hptc_' + OCXO_REFERENCE + '/img/hptc_' + OCXO_REFERENCE + '.tex'
    data = data.replace('HPTCIDX', OCXO_REFERENCE)
    data = data.replace('DATEIDX', str(datetime.datetime.now().date()))
    data = data.replace('TESTERIDX', AUTHOR_TESTER)
    data = data.replace('PLLCONFIGIDX', (name_config.replace('_','\_'))+'.slabtimeproj')
    with open(file_path, 'w') as f:
        f.write(data)
    # Convert .tex report to PDF
    
    inputted_path = r"\input{"+file_path+"}"
    os.system("pdflatex %s -quiet -output-directory=./reports/ -aux-directory=./reports/auxiliar" % inputted_path)


if __name__ == '__main__':
    # create logger with 'test_log' application
    logger_test_log = logging.getLogger('test_log')
    logger_test_log.setLevel(logging.DEBUG)
    # create file handler which logs even debug message
    fh = logging.FileHandler('./test_log_log.log')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)
    # create formatter and add it to the handlers
    formatter = logging.Formatter(
        '%(asctime)s - %(name)s - %(levelname)s: %(message)s',
        datefmt='%d-%m-%y %H:%M:%S')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)
    # add the handlers to the logger
    logger_test_log.addHandler(fh)
    logger_test_log.addHandler(ch)

    ## Open Visa Resource Manager
    rm_visa = visa.ResourceManager()
    power   = VisaComm(rm_visa, 'GPIB0::7::INSTR','test_log')

    ## Open Visa Resource Manager
    rm_visa2 = visa.ResourceManager()
    scope    = Scope(rm_visa2, 'USB0::0x0957::0x9002::MY48240177::0::INSTR', 'test_log')

    ## Open Visa Resource Manager
    rm_visa3 = visa.ResourceManager()
    PNA      = PhaseNoiseAnalyzer(rm_visa3, 'USB0::0x0AAD::0x011E::101281::0::INSTR', 'test_log')

    # Open PLL socket
    PLL = PLL_Si534xEVB([],'test_log')
    if(not ('nvm' in name_config) ): PLL.identify_devices()

    main(power, scope, PNA, PLL)
    del scope
    del PNA
    del power
